<?php
return [
    'user' => [
        'type' => 1,
        'description' => 'Користувач',
        'ruleName' => 'userGroup',
    ],
    'writer' => [
        'type' => 1,
        'description' => 'Блогер',
        'ruleName' => 'userGroup',
        'children' => [
            'user',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Адміністратор',
        'ruleName' => 'userGroup',
        'children' => [
            'writer',
        ],
    ],
];
