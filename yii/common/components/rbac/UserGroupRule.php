<?php

namespace common\components\rbac;

use Yii;
use yii\rbac\Rule;
use common\models\User;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';
    public function execute($user, $item, $params)
    {


        if (!Yii::$app->user->isGuest) {

            $group = Yii::$app->user->identity->group;

            if($item->name==User::$rolesName[User::GROUP_ADMIN]){

                return $group==User::GROUP_ADMIN;

            }elseif($item->name==User::$rolesName[User::GROUP_WRITER]){

                return $group==User::GROUP_ADMIN || $group==User::GROUP_WRITER;

            }elseif($item->name==User::$rolesName[User::GROUP_USER]){

                return $group==User::GROUP_ADMIN || $group==User::GROUP_WRITER || $group==User::GROUP_USER;

            }

        }

        return false;
    }
}
