<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 *
 * @property Articles[] $articles
 * @property Categories $parent
 * @property Categories[] $categories
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'name' => Yii::t('yii', 'Title'),
            'parent_id' => Yii::t('yii', 'Parent category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Articles::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['parent_id' => 'id']);
    }

    /**
     * @return array
     * Returns array of categories like: id => 'name'
     */
    public static function getCategoriesArray($excludeIds = [], $onlyMain = false, $includeMain = false)
    {
        if($onlyMain)
            $selectedCategories = Categories::find()->where(['parent_id' => 1])->select(['id', 'name', 'parent_id'])->asArray(true)->all();
        else
            $selectedCategories = Categories::find()->select(['id', 'name', 'parent_id'])->asArray(true)->all();
        $result = [];

        if($includeMain)
            $result[1] = Yii::t('yii', 'Main category');

        foreach ($selectedCategories as $category)
            if (!in_array($category['id'], $excludeIds))
                $result[$category['id']] = $category['name'];

        return $result;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     * Returns table of categories.
     */
    public function getCategoriesTable()
    {
        return Categories::find()->select(['id', 'name', 'parent_id'])->asArray(true)->all();
    }

    /**
     * @param $id
     * @return mixed
     * Returns name of category by id
     */
    static function getCategoryNameById($id)
    {
        if(!$id)
            return 'Main category';

        $categoryRow = Categories::findOne($id);

        return $categoryRow->name;
    }
}
