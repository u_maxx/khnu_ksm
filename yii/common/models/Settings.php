<?php

namespace common\models;

use Yii;
use common\models\Page;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property string $description
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'value', 'description'],
                'required',
                'message' => Yii::t('yii','This field can not be empty.')
            ],

            [
                ['name', 'value', 'description'],
                'string',
                'max' => 255,
            ]
        ];
    }

    public static function getPagesList($includeDefaultPage = true)
    {
        $pages = Page::getPageArray([], false);
        if($includeDefaultPage)
            $pages[0] = Yii::t('yii', 'Default Page');

        return $pages;
    }

    public static function getSetting($key)
    {
        $setting = static::findOne(['name' => $key]);
        if ($setting !== null) {
            return $setting->value;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'value' => Yii::t('app', 'Value'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
