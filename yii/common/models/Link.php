<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%links}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 */
class Link extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%links}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'trim'],
            [['title', 'url'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'title' => Yii::t('yii', 'Title'),
            'url' => Yii::t('yii', 'Url'),
        ];
    }
}
