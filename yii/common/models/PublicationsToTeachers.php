<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "publications_to_teachers".
 *
 * @property integer $teacher_id
 * @property integer $publication_id
 *
 * @property Publications $publication
 * @property Teachers $teacher
 */
class PublicationsToTeachers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publications_to_teachers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'publication_id'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['teacher_id', 'publication_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teacher_id' => Yii::t('yii', 'Teacher ID'),
            'publication_id' => Yii::t('yii', 'Publication ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublication()
    {
        return $this->hasOne(Publications::className(), ['id' => 'publication_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teachers::className(), ['id' => 'teacher_id']);
    }
}
