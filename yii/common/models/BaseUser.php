<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $login
 * @property string $email
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $group
 * @property integer $status
 * @property integer $is_teacher
 * @property string $created_at
 * @property string $updated_at
 * @property string $first_name
 * @property string $second_name
 * @property string $third_name
 * @property string $about
 *
 * @property Articles[] $articles
 * @property Files $files
 * @property Teachers $teachers
 */
class BaseUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['login', 'email', 'password_hash', 'password_reset_token', 'auth_key', 'group', 'status', 'is_teacher'],
                'required',
                'message' => Yii::t('yii','This field can not be empty.')
            ],
            [
                ['group', 'status', 'is_teacher'],
                'integer'
            ],
            [
                ['created_at', 'updated_at'],
                'safe'
            ],
            [
                ['about'],
                'string'
            ],
            [
                ['login', 'email', 'password_hash', 'password_reset_token', 'first_name', 'second_name', 'third_name'],
                'string',
                'max' => 255
            ],
            [
                ['auth_key'],
                'string',
                'max' => 32
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('app', 'Login'),
            'email' => Yii::t('app', 'Email'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'group' => Yii::t('app', 'Group'),
            'status' => Yii::t('app', 'Status'),
            'is_teacher' => Yii::t('app', 'Is Teacher'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'first_name' => Yii::t('app', 'First Name'),
            'second_name' => Yii::t('app', 'Second Name'),
            'third_name' => Yii::t('app', 'Third Name'),
            'about' => Yii::t('app', 'About'),
            'repeatpassword' => Yii::t('yii', 'Repeat password'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Articles::className(), ['author_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasOne(Files::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasOne(Teachers::className(), ['id' => 'id']);
    }


}
