<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use common\behaviors\TransliteSluggableBehavior;
use dosamigos\transliterator\TransliteratorHelper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $author_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $short_description
 * @property string $description
 * @property integer $is_published
 * @property string $slug
 *
 * @property Users $author
 * @property Categories $category
 */
class Articles extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'author_id', 'title', 'meta_description', 'meta_keyword', 'short_description', 'description', 'is_published'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['category_id', 'author_id', 'is_published'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['short_description', 'description'], 'string'],
            [['title', 'meta_description', 'meta_keyword', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['slug'], 'applyTransliteTable'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxSize' => '2000000']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'category_id' => Yii::t('yii', 'Category'),
            'author_id' => Yii::t('yii', 'Author'),
            'created_at' => Yii::t('yii', 'Created at'),
            'updated_at' => Yii::t('yii', 'Updated at'),
            'title' => Yii::t('yii', 'Title'),
            'meta_description' => Yii::t('yii', 'Meta Description'),
            'meta_keyword' => Yii::t('yii', 'Meta Keyword'),
            'short_description' => Yii::t('yii', 'Short Description'),
            'description' => Yii::t('yii', 'Description'),
            'is_published' => Yii::t('yii', 'Is Published'),
            'slug' => Yii::t('yii', 'Slug'),
            'imageFile' => Yii::t('yii', 'Image File'),
        ];
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TransliteSluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public function upload()
    {
        if ($this->validate()) {
            if($this->imageFile) {
                $this->image = Inflector::slug(TransliteratorHelper::process(time() . '-' . $this->imageFile->baseName)) . '.' . $this->imageFile->extension;
                $this->imageFile->saveAs('../../frontend/web/uploads/article-images/' . $this->image, false);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $attribute
     */
    public function applyTransliteTable($attribute)
    {
        $this->$attribute = Inflector::slug(TransliteratorHelper::process($this->$attribute));
    }

    /**
     * @return array
     * Returns associative array of 'slug' => 'title'
     */
    public static function getArrayArticleLinks() {
        $articles = Articles::find()->select(['title', 'id'])->asArray(true)->all();
        $articleLinks = [];
        foreach($articles as $article) {
            $articleLinks['/blog/index/'.$article['id']] = Yii::t('yii', 'Article') . ': ' . $article['title'];
        }
        return $articleLinks;
    }

    /**
     * @param $limit
     * @return array|string
     * Returns array of html blocks to use in carousel on main page
     */
    public static function getArrayOfItemsForLastNews($limit) {
        $articles = Articles::find()->where(['is_published' => 1])->limit($limit)->orderBy(['id' => SORT_DESC])->all();
        $countOfArticles = count($articles);
        $artciclesToShow = [];

        if(!$countOfArticles)
            return ['<div class="panel panel-danger">
                      <div class="panel-heading">
                        <h3 class="panel-title">'. Yii::t('yii', 'Error') .'</h3>
                      </div>
                      <div class="panel-body">
                        '. Yii::t('yii', 'We are sorry. There are no available news right now.') .'
                      </div>
                    </div>'];

        foreach($articles as $article) {
            $image = '';

            if($article->image)
                $image = Html::img('@web/uploads/article-images/' . $article->image, ['alt' => $article->title, 'style' => 'margin: 0 auto;', 'class' => 'img-short-news img-responsive img-hover']);

            array_push($artciclesToShow, '<div class="panel panel-default short-last-news">
                        <div class="panel-heading">
                            <h4 class="last-news-heading"><i class="glyphicon glyphicon-pushpin"></i> ' . $article->title . '</h4>
                        </div>'. $image .'
                        <div class="panel-body">
                            <p>' . $article->short_description . '</p>
                            <a class="btn btn-default" href="/blog/index/' . $article->id . '">Читати далі <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>');
        }

        return $artciclesToShow;

    }

    public static function getArticlesByCategory($categoryId) {
        $category = Categories::findOne($categoryId);
        $categoriesId = [$categoryId];

        if($category->parent_id === 1) {
            $subCategories = Categories::find()->where(['parent_id' => $categoryId])->all();
            foreach($subCategories as $subCategory) {
                array_push($categoriesId, $subCategory->id);
            }
            return Articles::find()->where(['category_id' => $categoriesId, 'is_published' => 1])->orderBy('id desc');
        } else {
            return Articles::find()->where(['category_id' => $categoryId, 'is_published' => 1])->orderBy('id desc');
        }


    }

    public static function countArticlesInCategory($categoryId) {
        $category = Categories::findOne($categoryId);

        if($category->parent_id === 1) {
            $countAll = Articles::find()->where(['category_id' => $categoryId, 'is_published' => 1])->count();
            $subCategories = Categories::find()->where(['parent_id' => $categoryId])->all();
            foreach($subCategories as $subCategory) {
                $countAll += Articles::find()->where(['category_id' => $subCategory->id, 'is_published' => 1])->count();
            }
            return $countAll;
        } else {
            return Articles::find()->where(['category_id' => $categoryId, 'is_published' => 1])->count();
        }
    }
}
