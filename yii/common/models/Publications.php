<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "publications".
 *
 * @property integer $id
 * @property string $title
 * @property string $year
 * @property string $publishing_house
 * @property integer $cout_page
 * @property string $about
 *
 * @property PublicationsToTeachers[] $publicationsToTeachers
 */
class Publications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'publishing_house', 'author' ], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['year'], 'integer'],
            [['about', 'cout_page'], 'string'],
            [['title', 'publishing_house', 'author'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'title' => Yii::t('yii', 'Title'),
            'year' => Yii::t('yii', 'Year of publication'),
            'publishing_house' => Yii::t('yii', 'Publishing house'),
            'cout_page' => Yii::t('yii', 'Count of pages'),
            'about' => Yii::t('yii', 'About publication'),
            'author' => Yii::t('yii', 'Author'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicationsToTeachers()
    {
        return $this->hasMany(PublicationsToTeachers::className(), ['publication_id' => 'id']);
    }

    /**
     * @return mixed
     * Returns array of years to use it in drop down list
     */
    public static function getYearsArray($fromYear = 1920)
    {
        $thisYear = date('Y', time());

        for($yearNum = $thisYear; $yearNum >= $fromYear; $yearNum--){
            $years[$yearNum] = $yearNum;
        }

        return $years;
    }

    /**
     * @return array
     * Returns array of publications like 'id' => 'title' to show it in drop down list
     */
    public static function getArrayOfPublications()
    {
        $publications = static::find()->select(['id', 'title', 'author'])->asArray(true)->all();
        $arrayOfPublications = [];
        foreach ($publications as $currentPublication)
            $arrayOfPublications[$currentPublication['id']] = $currentPublication['author'] . ' / ' . $currentPublication['title'];
        return $arrayOfPublications;

    }
}
