<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

class User extends BaseUser implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    public static $statusesName=[
        'deleted',
        'active',
    ];

    const GROUP_USER = 0;
    const GROUP_WRITER = 1;
    const GROUP_ADMIN = 2;

    public static $rolesName=[
        'user',
        'writer',
        'admin'
    ];

    const TEACHER_TRUE = 1;
    const TEACHER_FALSE = 0;


    /**
     * @var string
     * Use for receive password from backend create && update actions
     */
    public $password;
    public $repeatpassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules=parent::rules();

        $rules[]=[
            ['status'],
            'default',
            'value' => self::STATUS_ACTIVE,
        ];

        $rules[]=[
            ['status'],
            'in',
            'range' => [
                self::STATUS_ACTIVE,
                self::STATUS_DELETED
            ]
        ];

        $rules[]=[
            ['group'],
            'default',
            'value' => self::GROUP_USER
        ];

        $rules[]=[
            ['group'],
            'in',
            'range' => [
                self::GROUP_USER,
                self::GROUP_WRITER,
                self::GROUP_ADMIN
            ]
        ];

        $rules[]=[
            ['password'],
            'required',
            'message' => Yii::t('yii','This field can not be empty.'),
            'on'=>'admin_create'
        ];

        $rules[]=[
            ['password'],
            'string',
            'min'=>4,
            'on'=>[
                'admin_create',
                'admin_update',
                'user_update'
            ]
        ];

        $rules[]=[
            'repeatpassword',
            'compare',
            'compareAttribute' => 'password',
            'message' => Yii::t('yii', 'Passwords don\'t match'),
            'on'=>'user_update'
        ];

        return $rules;
    }

    public function scenarios(){
        $scenarios=parent::scenarios();

        $scenarios['admin_create']=[
            'login',
            'email',
            'password',
            'group',
            'status',
            'is_teacher',
            'first_name',
            'second_name',
            'third_name',
            'about'
        ];

        $scenarios['user_update']=[
            'email',
            'password',
            'repeatpassword',
            'first_name',
            'second_name',
            'third_name',
            'about'
        ];

        $scenarios['admin_update']=$scenarios['admin_create'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'login' => $username,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Enter full name
     */

    public function setFullName($first, $second, $third)
    {
        $this->first_name=$first;
        $this->second_name=$second;
        $this->third_name=$third;
    }

    public function attributeLabels()
    {
        $res=parent::attributeLabels();

        $res['password']  =  Yii::t('app', 'Password');

        return $res;
    }


    /*
     * Get translated group name by index
     */
    public static function translatedGroupName($index)
    {
        return Yii::t('app', self::$rolesName[$index]);
    }

    /*
     * Get translated group name by index
     */

    public static function translatedStatusName($index)
    {
        return Yii::t('app', self::$statusesName[$index]);
    }

    /**
     * @param $id
     * @return string
     * Returns first name, last name and nickname of only user by id
     */
    public static function getUserNameById($id)
    {
        $userRow = User::findOne($id);

        if(!$userRow->first_name || !$userRow->second_name) {
            $name =Yii::t('yii', 'Unnamed user');
            $result = sprintf("%s (%s)", Yii::t('yii', $name), $userRow->login);
        } else
            $result = sprintf("%s %s (%s)", $userRow->first_name, $userRow->second_name, $userRow->login);

        return $result;
    }


}