<?php

namespace common\models;

use Yii;
use Faker\Provider\DateTime;
use yii\web\UploadedFile;
use dosamigos\transliterator\TransliteratorHelper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "teachers".
 *
 * @property integer $id
 * @property string $degree
 * @property string $name
 * @property string $email
 * @property string $image
 * @property string $about
 *
 * @property PublicationsToTeachers[] $publicationsToTeachers
 * @property ScActivitiesToTeachers[] $scActivitiesToTeachers
 * @property SubjectsToTeachers[] $subjectsToTeachers
 */
class Teachers extends \yii\db\ActiveRecord
{
    public $scActivitiesArray;
    public $publicationsArray;
    public $subjectsArray;
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teachers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['degree', 'name', 'about'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['about'], 'string'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            [['scActivitiesArray', 'publicationsArray', 'subjectsArray'], 'safe'],
            [['degree', 'name', 'email', 'image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'degree' => Yii::t('yii', 'Degree'),
            'name' => Yii::t('yii', 'Name'),
            'email' => Yii::t('yii', 'Email'),
            'image' => Yii::t('yii', 'Lecturer\'s photo'),
            'about' => Yii::t('yii', 'About lecturer'),
            'scActivitiesArray' => Yii::t('yii', 'Scientific activities'),
            'subjectsArray' => Yii::t('yii', 'Subjects'),
            'publicationsArray' => Yii::t('yii', 'Publications'),
            'imageFile' => Yii::t('yii', 'Image File'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicationsToTeachers()
    {
        return $this->hasMany(PublicationsToTeachers::className(), ['teacher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScActivitiesToTeachers()
    {
        return $this->hasMany(ScActivitiesToTeachers::className(), ['teacher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectsToTeachers()
    {
        return $this->hasMany(SubjectsToTeachers::className(), ['teacher_id' => 'id']);
    }

    /**
     * @param bool $getTitles
     * @return array
     * Returns array with all id of Current teacher's scientific activities if $getTitles == false
     * or returns array with all names of Current teacher's scientific activities if $getTitles == true
     */
    public function getArrayOfCurTeacherScActivities($getTitles = false)
    {
        $arrayOfCurTeacherScActivities = [];

        if(!$getTitles)
            foreach ($this->scActivitiesToTeachers as $curScActivity)
                array_push($arrayOfCurTeacherScActivities, $curScActivity->sc_activity_id);
        else
            foreach ($this->scActivitiesToTeachers as $curScActivity)
                array_push($arrayOfCurTeacherScActivities, $curScActivity->scActivity->name);

        return $arrayOfCurTeacherScActivities;
    }

    /**
     * @param bool $getTitles
     * @return array
     * Returns array with all id of Current teacher's subjects if $getTitles == false
     * or returns array with all names of Current teacher's subjects if $getTitles == true
     */
    public function getArrayOfCurTeacherSubjects($getTitles = false)
    {
        $arrayOfCurTeacherSubjects = [];

        if(!$getTitles)
            foreach($this->subjectsToTeachers as $curSubject)
                array_push($arrayOfCurTeacherSubjects, $curSubject->subject_id);
        else
            foreach($this->subjectsToTeachers as $curSubject)
                array_push($arrayOfCurTeacherSubjects, $curSubject->subject->name);

        return $arrayOfCurTeacherSubjects;
    }

    /**
     * @param bool $getTitles
     * @return array
     * Returns array with all id of Current teacher's publications if $getTitles == false
     * or returns array with all names of Current teacher's publications if $getTitles == true
     */
    public function getArrayOfCurTeacherPublications($getTitles = false)
    {
        $arrayOfCurTeacherPublications = [];

        if(!$getTitles)
            foreach($this->publicationsToTeachers as $curPublications)
                array_push($arrayOfCurTeacherPublications, $curPublications->publication_id);
        else
            foreach($this->publicationsToTeachers as $curPublications)
                array_push($arrayOfCurTeacherPublications, $curPublications->publication->title);

        return $arrayOfCurTeacherPublications;
    }

    /**
     * @return bool
     * @throws \Exception
     * Save selected publications from drop down list
     */
    public function savePublications()
    {
        if (!$this->getIsNewRecord())
            while($publicationToTeacher = PublicationsToTeachers::find()->where(['teacher_id' => $this->id])->one())
                if(!$publicationToTeacher->delete())
                    return false;

        if ($this->publicationsArray)
            foreach($this->publicationsArray as $publicationId) {
                $modePublication = new PublicationsToTeachers();
                $modePublication->teacher_id = $this->id;
                $modePublication->publication_id = $publicationId;
                if(!$modePublication->save())
                    return false;
            }

        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     * Save selected subjects from drop down list
     */
    public function saveSubjects()
    {
        if (!$this->getIsNewRecord())
            while($subjectToTeacher = SubjectsToTeachers::find()->where(['teacher_id' => $this->id])->one())
                if(!$subjectToTeacher->delete())
                    return false;

        if ($this->subjectsArray)
            foreach($this->subjectsArray as $subjectId) {
                $modeSubject = new SubjectsToTeachers();
                $modeSubject->teacher_id = $this->id;
                $modeSubject->subject_id = $subjectId;
                if(!$modeSubject->save())
                    return false;
            }

        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     * Save selected scientific activities from drop down list
     */
    public function saveScActivities()
    {
        if (!$this->getIsNewRecord())
            while($scActivityToTeacher = ScActivitiesToTeachers::find()->where(['teacher_id' => $this->id])->one())
                if(!$scActivityToTeacher->delete())
                    return false;

        if ($this->scActivitiesArray)
            foreach($this->scActivitiesArray as $scActivityId) {
                $modeScActivity = new ScActivitiesToTeachers();
                $modeScActivity->teacher_id = $this->id;
                $modeScActivity->sc_activity_id = $scActivityId;
                if(!$modeScActivity->save())
                    return false;
            }

        return true;
    }

    public function saveTeacher()
    {
        if($this->imageFile) {
            $this->image = Inflector::slug(TransliteratorHelper::process(time() . '-' . $this->imageFile->baseName)) . '.' . $this->imageFile->extension;
            if(!$this->save())
                return false;
            $this->imageFile->saveAs('../../frontend/web/uploads/teachers-photos/' . $this->image, false);
        } elseif(!$this->save())
            return false;
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * Redefinition of ActiveRecord method
     * Will be called by default after save() method is done success
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->saveSubjects();
        $this->saveScActivities();
        $this->savePublications();

    }
}
