<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sc_activities_to_teachers".
 *
 * @property integer $teacher_id
 * @property integer $sc_activity_id
 *
 * @property ScientificActivities $scActivity
 * @property Teachers $teacher
 */
class ScActivitiesToTeachers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_activities_to_teachers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'sc_activity_id'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['teacher_id', 'sc_activity_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teacher_id' => Yii::t('yii', 'Teacher ID'),
            'sc_activity_id' => Yii::t('yii', 'Sc Activity ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScActivity()
    {
        return $this->hasOne(ScientificActivities::className(), ['id' => 'sc_activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teachers::className(), ['id' => 'teacher_id']);
    }
}
