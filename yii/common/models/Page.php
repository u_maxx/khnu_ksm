<?php

namespace common\models;

use Yii;
use common\behaviors\TransliteSluggableBehavior;
use dosamigos\transliterator\TransliteratorHelper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $short_description
 * @property string $description
 * @property integer $is_published
 * @property string $slug
 */
class Page extends \yii\db\ActiveRecord
{
    const PAGE_HIDDEN = 0;
    const PAGE_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'meta_description', 'meta_keyword', 'short_description', 'description'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['short_description', 'description'], 'string'],
            [['is_published'], 'boolean'],
            [['title', 'meta_description', 'meta_keyword', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['slug'], 'applyTransliteTable'],
        ];
    }

    public function applyTransliteTable($attribute, $params)
    {
        $this->$attribute = Inflector::slug(TransliteratorHelper::process($this->$attribute));
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TransliteSluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'title' => Yii::t('yii', 'Title'),
            'meta_description' => Yii::t('yii', 'Meta Description'),
            'meta_keyword' => Yii::t('yii', 'Meta Keyword'),
            'short_description' => Yii::t('yii', 'Short Description'),
            'description' => Yii::t('yii', 'Description'),
            'is_published' => Yii::t('yii', 'Is Published?'),
            'slug' => Yii::t('yii', 'Slug'),
        ];
    }

    /**
     * Get array of page in format page_id => page_title to render in a select box
     */
    public static function getPageArray($excludeIds = [], $includeRootPage = true)
    {
        $pages = static::find()->select(['id', 'title'])->asArray(true)->all();
        $result = [];

        if ($includeRootPage) {
            $result[0] = Yii::t('app', 'Root Page');
        }

        foreach ($pages as $page) {
            if (!in_array($page['id'], $excludeIds)) {
                $result[$page['id']] = $page['title'];
            }
        }

        return $result;
    }


    /**
     * @return array
     * Returns associative array of 'link' => 'title'
     */
    public static function getArrayPagesLinks() {
        $pages = static::find()->select(['title', 'slug'])->asArray(true)->all();
        $pagesLinks = [];
        foreach($pages as $page) {
            $pagesLinks['/'.$page['slug']] = Yii::t('yii', 'Page') . ': ' . $page['title'];
        }
        return $pagesLinks;
    }
}
