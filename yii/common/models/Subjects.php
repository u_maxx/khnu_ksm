<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subjects".
 *
 * @property integer $id
 * @property string $name
 * @property string $about
 *
 * @property SubjectsToTeachers[] $subjectsToTeachers
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subjects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'about'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['about'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'name' => Yii::t('yii', 'Title'),
            'about' => Yii::t('yii', 'About subject'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectsToTeachers()
    {
        return $this->hasMany(SubjectsToTeachers::className(), ['subject_id' => 'id']);
    }

    /**
     * @return array
     * Returns array of subjects like 'id' => 'name' to show it in drop down list
     */
    public static function getArrayOfSubjects()
    {
        $subjects = static::find()->select(['id', 'name'])->asArray(true)->all();
        $arrayOfSubjects = [];
        foreach($subjects as $currentSubject)
            $arrayOfSubjects[$currentSubject['id']] = $currentSubject['name'];
        return $arrayOfSubjects;
    }
}
