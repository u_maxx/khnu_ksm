<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property string $description
 */
class Rules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'value', 'description'],
                'required',
                'message' => Yii::t('yii','This field can not be empty.')
            ],

            [
                ['name',  'description'],
                'string',
                'max' => 255,
            ],
            [
                ['value'],
                'string',
            ]
        ];
    }

    public static function getRule($key)
    {
        $rules = static::findOne(['name' => $key]);
        if ($rules !== null) {
            return $rules->value;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'value' => Yii::t('yii', 'Registration rules'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
