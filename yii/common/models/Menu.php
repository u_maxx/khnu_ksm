<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%menu}}".
 *
 * @property integer $id
 * @property integer $item_type
 * @property integer $item_id
 * @property integer $parent_id
 */
class Menu extends \yii\db\ActiveRecord
{

    const MENU_ITEM_TYPE_PAGE = 0;
    const MENU_ITEM_TYPE_LINK = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_type', 'item_id', 'parent_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'item_type' => Yii::t('yii', 'Item Type'),
            'item_id' => Yii::t('yii', 'Item ID'),
            'parent_id' => Yii::t('yii', 'Parent ID'),
        ];
    }

    protected static function saveItem($item, $parentId = 0)
    {
        $menuItem = new Menu();
        $menuItem->item_id = $item->item_id;
        $menuItem->item_type = $item->item_type;
        $menuItem->parent_id = $parentId;
        $menuItem->save();

        if ($item->children && is_array($item->children)) {
            foreach ($item->children as $child) {
                static::saveItem($child, $menuItem->id);
            }
        }
    }

    public static function getAvalaibleItems()
    {
        $usedPages = Menu::find()->select('item_id')->where(['item_type' => static::MENU_ITEM_TYPE_PAGE]);
        $usedLinks = Menu::find()->select('item_id')->where(['item_type' => static::MENU_ITEM_TYPE_LINK]);

        $pageTypeExpression = new Expression("'" . static::MENU_ITEM_TYPE_PAGE . "' AS 'item_type'");
        $linkTypeExpression = new Expression("'" . static::MENU_ITEM_TYPE_LINK . "' AS 'item_type'");

        $pages = Page::find()->select(['pages.id', 'pages.title', $pageTypeExpression])
            ->where(['pages.is_published' => Page::PAGE_PUBLISHED])
            ->andWhere(['not in', 'pages.id', $usedPages])
            ->asArray()
            ->all();

        $links = Link::find()->select(['links.id', 'links.title', $linkTypeExpression])
            ->where(['not in', 'links.id', $usedLinks])
            ->asArray()
            ->all();

        return array_merge($pages, $links);
    }

    public static function getItems()
    {
        $pagesFromMenu = Menu::find()
            ->select([
                'menu.id',
                'pages.title',
                'url' => 'pages.slug',
                'menu.item_id',
                'menu.item_type',
                'menu.parent_id'
            ])
            ->innerJoin('pages', 'menu.item_id=pages.id')
            ->where(['menu.item_type' => Menu::MENU_ITEM_TYPE_PAGE])
            ->asArray()
            ->all();

        for ($i = 0; $i < count($pagesFromMenu); $i++) {
            $pagesFromMenu[$i]['url'] = '/'.$pagesFromMenu[$i]['url'];
        }

        $linksFromMenu = Menu::find()
            ->select([
                'menu.id',
                'links.title',
                'links.url',
                'menu.item_id',
                'menu.item_type',
                'menu.parent_id'
            ])
            ->innerJoin('links', 'menu.item_id=links.id')
            ->where(['menu.item_type' => Menu::MENU_ITEM_TYPE_LINK])
            ->asArray()
            ->all();

        $menu = array_merge($pagesFromMenu, $linksFromMenu);

        // the items of menu array should be sorted by parent_id key to this algorithm works well
        ArrayHelper::multisort($menu, "parent_id", SORT_ASC);

        $preparedMenuItems = [
            0 => [],
        ];

        foreach ($menu as $item) {
            $id = $item['id'];
            $parentId = $item['parent_id'];
            if (array_key_exists($parentId, $preparedMenuItems)) {
                $preparedMenuItems[$parentId]['children'][] = $id;
                $preparedMenuItems[$id] = $item;
            }
        }

        return self::prepareMenuItem($preparedMenuItems);
    }

    private static function prepareMenuItem($preparedItems, $parentId = 0)
    {
        $item = $preparedItems[$parentId];

        $currentMenuItem = [];

        if ($parentId) {
            $currentMenuItem = [
                'id' => $item['id'],
                'label' => $item['title'],
                'url' => $item['url'],
                'item_id' => $item['item_id'],
                'item_type' => $item['item_type'],
            ];
        }

        if (array_key_exists('children', $item) && is_array($item['children'])) {
            if ($parentId) {
                foreach ($item['children'] as $child) {
                    $currentMenuItem['items'][] = static::prepareMenuItem($preparedItems, $child);
                }
            } else {
                foreach ($item['children'] as $child) {
                    $currentMenuItem[] = static::prepareMenuItem($preparedItems, $child);
                }
            }
        }

        return $currentMenuItem;
    }

    public static function saveFromJSON($jsonedMenu)
    {
        $decodedMenu = json_decode($jsonedMenu);
        Menu::deleteAll();
        foreach ($decodedMenu as $item) {
            static::saveItem($item);
        }
    }

}
