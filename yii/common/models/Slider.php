<?php

namespace common\models;

use Yii;
use dosamigos\transliterator\TransliteratorHelper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $link
* @property integer $show_in_main
 */
class Slider extends \yii\db\ActiveRecord
{
    public $imageFile;
    public $selectLink;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'show_in_main'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['title', 'description', 'link'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxSize' => '2000000']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'title' => Yii::t('yii', 'Title'),
            'description' => Yii::t('yii', 'Description'),
            'imageFile' => Yii::t('yii', 'Slide image'),
            'image' => Yii::t('yii', 'Slide image'),
            'link' => Yii::t('yii', 'Link'),
            'show_in_main' => Yii::t('yii', 'Show in main page')
        ];
    }


    /**
     * @return bool
     */
    public function saveCarousel()
    {
        if($this->imageFile) {
            $this->image = Inflector::slug(TransliteratorHelper::process(time() . '-' . $this->imageFile->baseName)) . '.' . $this->imageFile->extension;
            if(!$this->save())
                return false;
            $this->imageFile->saveAs('../../frontend/web/uploads/carousel-images/' . $this->image, false);
        } elseif(!$this->save())
            return false;
        return true;
    }

    public static function getArrayOfActiveSlides() {
        $slides = static::find()->where(['show_in_main' => 1])->all();
        return $slides;
    }

}
