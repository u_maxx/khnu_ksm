<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "scientific_activities".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ScActivitiesToTeachers[] $scActivitiesToTeachers
 */
class ScientificActivities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scientific_activities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'name' => Yii::t('yii', 'Scientific activity name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScActivitiesToTeachers()
    {
        return $this->hasMany(ScActivitiesToTeachers::className(), ['sc_activity_id' => 'id']);
    }

    /**
     * @return array
     * Returns array of Scientific activities like 'id' => 'name' to show it in drop down list
     */
    public static function getArrayOfScActivities()
    {
        $scActivities = static::find()->select(['id', 'name'])->asArray(true)->all();
        $arrayOfScActivities = [];
        foreach($scActivities as $currentScActivity) {
            $arrayOfScActivities[$currentScActivity['id']] = $currentScActivity['name'];
        }

        return $arrayOfScActivities;
    }
}
