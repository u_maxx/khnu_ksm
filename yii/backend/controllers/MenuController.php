<?php

namespace backend\controllers;

use Yii;
use common\models\Menu;
use yii\filters\AccessControl;

class MenuController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'save'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $menu = Menu::getItems();
        $availableItems = Menu::getAvalaibleItems();

        return $this->render('index', [
            'availableItems' => $availableItems,
            'menuItems' => $menu,
        ]);
    }

    public function actionSave()
    {
        try {
            Menu::saveFromJSON(Yii::$app->request->post('menu'));

            Yii::$app->getSession()->setFlash('success', Yii::t('yii', 'The menu has been saved.'));
        } catch (\Exception $ex) {
            Yii::$app->getSession()->setFlash('error', $ex->getMessage());
        }
    }

}
