<?php

namespace backend\controllers;

use Yii;
use common\models\Settings;
use common\models\Rules;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models=Settings::find()->all();
        $rules=Rules::findOne(['name' => 'registration_rules']);

        if (Settings::loadMultiple($models, Yii::$app->request->post()) && Settings::validateMultiple($models)) {

            foreach ($models as $model) {
                $model->save();
            }

            if ($rules->load(Yii::$app->request->post()) && $rules->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('yii', 'Settings has been saved.'));
                return $this->render('update', [
                    'models' => $models,
                    'rules' => $rules
                ]);
            }

        } else {
            return $this->render('update', [
                'models' => $models,
                'rules' => $rules
            ]);
        }
    }

    /**
     * Finds the Settings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'The requested page does not exist.'));
        }
    }
}
