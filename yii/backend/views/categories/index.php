<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('yii', 'Blog: Categories');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="categories-index ">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Create category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= '<div class="categories-tree">' ?>
    <?= Html::tag('p',  Yii::t('yii','Main category')) ?>
    <?php
        echo '<ol>';

        foreach($categoriesTable as $category)
        {
            if($category['parent_id'] === '1') {
                echo '<li>'. Html::a(Html::encode($category['name']), ['view', 'id' => $category['id']]) . '</li>';
                echo '<ol>';

                foreach($categoriesTable as $subCategory)
                {
                    if($category['id'] == $subCategory['parent_id']) {
                        echo '<li>' . Html::a(Html::encode($subCategory['name']), ['view', 'id' => $subCategory['id']]) . '</li>';
                    }
                }

                echo '</ol>';
            }
        }

        echo '</ol>';
    ?>

    <?= '</div>' ?>

</div>
