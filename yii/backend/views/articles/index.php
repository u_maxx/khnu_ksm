<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('yii', 'Blog: Articles');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="articles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Create Article'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('yii', 'Edit categories'), ['/categories'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'title',
            [
                'attribute' =>  'category_id',
                'value' =>  function($model) {
                    return Categories::getCategoryNameById($model->category_id);
                },
            ],

            [
                'attribute' =>  'author_id',
                'value' =>  function($model) {
                    return User::getUserNameById($model->author_id);
                },
            ],

            [
                'attribute' =>  'created_at',
                'value' =>  function($model) {
                    return sprintf("%s / %s", Yii::$app->formatter->asDate($model->created_at, 'long'), Yii::$app->formatter->asTime($model->created_at, 'long'));
                },
            ],
            //'updated_at',
            // 'meta_description',
            // 'meta_keyword',
            // 'short_description:ntext',
            // 'description:ntext',
            [
                'attribute' =>  'is_published',
                'value' =>  function($model) {
                    return ($model->is_published) ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                },
            ],
            'slug',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>



</div>
