<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Categories;
use common\models\User;
use backend\assets\PageAssets;
use backend\assets\TinyMCEAssets;

/* @var $this yii\web\View */
/* @var $model common\models\Articles */
/* @var $form yii\widgets\ActiveForm */

TinyMCEAssets::register($this);
PageAssets::register($this);
?>



<div class="articles-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(Categories::getCategoriesArray()) ?>

    <?php if($model->isNewRecord): ?>

        <?= $form->field($model, 'author_id')->dropDownList([
            Yii::$app->user->getId() => User::getUserNameById(Yii::$app->user->getId())
        ]) ?>

    <?php endif; ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?php if(!$model->isNewRecord)
        if($model->image)
            echo Html::img('/uploads/article-images/' . $model->image, ['alt' => $model->title, 'class' => 'thumbnail', 'style' => 'max-width: 600px;']) ;
        else
            echo Html::tag('p', Yii::t('yii', 'There is no image loaded for this article.'), ['class' => 'bg-info', 'style' => 'padding: 20px 30px; font-style: italic;']);
    ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'meta_keyword')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_published')->dropDownList([
        0   =>  Yii::t('app', 'no'),
        1   =>  Yii::t('app', 'yes'),
    ]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
