<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $model common\models\Articles */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Blog: Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="articles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('yii', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' =>  'category_id',
                'value' => Categories::getCategoryNameById($model->category_id),

            ],

            [
                'attribute' =>  'author_id',
                'value' => User::getUserNameById($model->author_id),

            ],
            [
                'attribute' =>  'created_at',
                'value' => sprintf("%s / %s", Yii::$app->formatter->asDate($model->created_at, 'long'), Yii::$app->formatter->asTime($model->created_at, 'long')),
            ],
            [
                'attribute' =>  'updated_at',
                'value' => sprintf("%s / %s", Yii::$app->formatter->asDate($model->updated_at, 'long'), Yii::$app->formatter->asTime($model->updated_at, 'long')),
            ],
            'meta_description',
            'meta_keyword',
            'short_description:html',
            'description:html',
            [
                'attribute' =>  'is_published',
                'value' =>  ($model->is_published)? Yii::t('app', 'Yes') : Yii::t('app', 'No'),
            ],
            'slug',
            [
                'attribute' =>  'image',
                'value' => ($model->image)? '/uploads/article-images/' . $model->image  : Yii::t('yii', 'There is no image loaded for this article.'),
                'format' => ($model->image)? ['image',['style'=>'max-width: 800px;', 'alt' => $model->title]] : 'text',
            ]
        ],
    ]) ?>

</div>
