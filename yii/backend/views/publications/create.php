<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Publications */

$this->title = Yii::t('yii', 'Create publication');
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Lecturers: Publications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publications-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
