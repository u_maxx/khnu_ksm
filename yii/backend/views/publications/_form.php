<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\PageAssets;
use \backend\assets\TinyMCEAssets;
use common\models\publications;

/* @var $this yii\web\View */
/* @var $model common\models\Publications */
/* @var $form yii\widgets\ActiveForm */

TinyMCEAssets::register($this);
PageAssets::register($this);
?>

<div class="publications-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'publishing_house')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'year')->textInput()->dropDownList(Publications::getYearsArray(), ['prompt'=> Yii::t('yii','Select year')]) ?>

    <?= $form->field($model, 'cout_page')->textInput()->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'about')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
