<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Publications */

$this->title = Yii::t('yii', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('yii', 'publication'),
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Lecturers: Publications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('yii', 'Update');
?>
<div class="publications-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
