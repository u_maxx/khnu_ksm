<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\models\Settings;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$siteFullName = Settings::getSetting('site_name');
$siteShortName = Settings::getSetting('site_title');

$this->title = $siteFullName . ' - ' . Yii::t('yii', 'Admin panel');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
    <?php $this->head() ?>  
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => $siteShortName,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => Yii::t('yii', 'Login'), 'url' => ['/site/login']];
            } else {
                $menuItems = [
                    ['label' => Yii::t('app', 'Home'), 'url' => ['/']],
                    ['label' => Yii::t('app', 'CMS'), 'url' => ['#'],
                        'items' => [
                            ['label' => Yii::t('app', 'CMS'), 'url' => ['/page']],
                            ['label' => Yii::t('yii', 'Menu'), 'url' => ['/menu']],
                            ['label' => Yii::t('yii', 'Links'), 'url' => ['/link']],
                            ['label' => Yii::t('yii', 'Slider'), 'url' => ['/slider']],
                        ]
                    ],
                    ['label' => Yii::t('yii', 'Blog'), 'url' => ['#'],
                        'items' => [
                            ['label' => Yii::t('yii', 'Articles'), 'url' => ['/articles']],
                            ['label' => Yii::t('yii', 'Categories'), 'url' => ['/categories']],
                        ],
                    ],
                    ['label' => Yii::t('yii', 'Lecturers'), 'url' => ['#'],
                        'items' => [
                            ['label' => Yii::t('yii', 'Biographies'), 'url' => ['/teachers']],
                            ['label' => Yii::t('yii', 'Subjects'), 'url' => ['/subjects']],
                            ['label' => Yii::t('yii', 'Publications'), 'url' => ['/publications']],
                            ['label' => Yii::t('app', 'Scientific activities'), 'url' => ['/scientific-activities']]
                        ],
                    ],
                    ['label' => Yii::t('app', 'Users'), 'url' => ['/user']],
                    ['label' => Yii::t('app', 'Settings'), 'url' => ['/settings']],
                    ['label' => Yii::t('app', 'Logout ({username})', [
                            'username' => Yii::$app->user->identity->login,
                        ]),
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ],
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; KSM <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
