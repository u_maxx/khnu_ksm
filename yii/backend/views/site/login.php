<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('yii', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p> <?php echo Yii::t('yii', 'Please fill out the following fields to login:') ?> </p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'login') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('yii', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
