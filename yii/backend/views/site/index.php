<?php
/* @var $this yii\web\View */

$this->title = 'KSM Admin';
?>
<!-- Facebook -->
<div id="fb-root"></div>

<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/uk_UA/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Facebook -->
<!-- VK -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>
<!-- VK -->

<!-- Twitter -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<!-- Twitter -->

<div class="site-index">
    <div class="body-content" style="margin-top: 50px;">
        <h1 style="margin-bottom: 30px;"><span class="glyphicon glyphicon-dashboard"></span> KSM Dashboard</h1>
        <div class="row thumbnail dashboard-container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div class="input-group dashboard-item">
                    <!-- VK Widget -->
                    <div id="vk_groups"></div>
                    <script type="text/javascript">
                        VK.Widgets.Group("vk_groups", {mode: 2, width: "300", height: "500"}, 95427377);
                    </script>
                    <!-- VK Widget -->
                </div>
                <div style="margin-bottom: 30px;"></div>
            </div>
            <div class="clear"></div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div class="input-group dashboard-item">
                    <!-- Facebook -->
                    <div class="fb-page" data-href="https://www.facebook.com/khnuksm"
                         data-small-header="true"
                         data-adapt-container-width="true"
                         data-hide-cover="true"
                         data-show-facepile="true"
                         data-show-posts="true"
                         data-width="300"
                         data-height="500">
                        <div class="fb-xfbml-parse-ignore">
                            <blockquote cite="https://www.facebook.com/khnuksm"><a href="https://www.facebook.com/khnuksm">Кафедра комп&#039;ютерних систем та мереж ХНУ</a></blockquote>
                        </div>
                    </div>
                    <!-- Facebook -->
                </div>
                <div style="margin-bottom: 30px;"></div>
            </div>
            <div class="clear"></div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div class="input-group dashboard-item">
                    <a class="twitter-timeline" href="https://twitter.com/ksm_khnu"
                       data-widget-id="658410840819912708"
                       width="300"
                       height="500">Твіти від @ksm_khnu</a>
                </div>
                <div style="margin-bottom: 30px;"></div>
            </div>
        </div>
    </div>
</div>
