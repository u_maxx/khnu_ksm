<?php
/* @var $this yii\web\View */

use backend\assets\MenuAssets;
use \yii\helpers\Html;

MenuAssets::register($this);

//todo: extract the following function into a widget.
function renderNestedList($item)
{
    printf('<li class="dd-item" data-id="%s" data-item_id="%s" data-item_type="%s">', $item['id'], $item['item_id'], $item['item_type']);
    printf('<div class="dd-handle">%s</div>', $item['label']);
    if (array_key_exists('items', $item) && is_array($item['items'])) {
        printf('<ol class="dd-list">');
        foreach ($item['items'] as $child) {
            //var_dump($child);
            renderNestedList($child);
        }
        printf('</ol>');
    }
    printf('</li>');
}

function renderMenuItems($menuItems)
{
    if (count($menuItems) > 0) {
        printf('<ol class="dd-list">');
        foreach ($menuItems as $item) {
            renderNestedList($item);
        }
        printf('</ol>');
    } else {
        printf('<div class="dd-empty"></div>');
    }
}

$this->title = Yii::t('yii', 'Menu');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<h1><?= Yii::t('yii', 'Manage menu'); ?></h1>
<hr>
<div class="row">
    <div class="col-md-6">
        <div class="text-center">
            <?= Yii::t('yii', 'Menu Items'); ?>
        </div>
        <div id="menu_items" class="dd">
            <?php renderMenuItems($menuItems) ?>
        </div>
        <div class="result">

        </div>
    </div>
    <div class="col-md-6">
        <div class="text-center">
            <?= Yii::t('yii', 'Available Items'); ?>
        </div>
        <div id="available_items" class="dd">
            <?php if (count($availableItems) > 0): ?>
                <ol class="dd-list">
                    <?php foreach ($availableItems as $item): ?>
                        <li class="dd-item" data-item_id="<?= $item['id']; ?>"
                            data-item_type="<?= $item['item_type']; ?>">
                            <div class="dd-handle"><?= $item['title']; ?></div>
                        </li>
                    <?php endforeach ?>
                </ol>
            <?php else: ?>
                <div class="dd-empty">
                    <span>
                        <?= Yii::t('yii', 'There are no available <a href="link">links</a> or <a href="page">pages</a> to add to menu'); ?>
                    </span>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="center-block">
        <button id="save-menu" class="btn btn-success"><?= Yii::t('yii', 'Save'); ?></button>
    </div>
</div>