<?php
use common\models\Settings;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\PageAssets;
use backend\assets\TinyMCEAssets;

/* @var $this yii\web\View */
/* @var $models common\models\Settings[] */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Update {modelClass} ', [
    'modelClass' => Yii::t('yii','settings'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

TinyMCEAssets::register($this);
PageAssets::register($this);
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="settings-update">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="settings-form">

        <?php if(count($models)>0): ?>

            <?php $form = ActiveForm::begin(); ?>

            <?php foreach ($models as $i => $model): ?>
                <?php echo $form->field($model, '[' . $i . ']name')->hiddenInput()->label(false) ?>

                <?php if ($model->name === 'home_page_id'): ?>
                    <?php echo $form->field($model, '[' . $i . ']value')->dropDownList(Settings::getPagesList())->label($model->description . " [ " . $model->name . " ] "); ?>
                <?php elseif($model->name === 'blog_post_per_page') : ?>
                    <?php echo $form->field($model, '[' . $i . ']value')->dropDownList([5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30, 40 => 40, 50 => 50, 75 => 75, 100 => 100])->label($model->description . " [ " . $model->name . " ] "); ?>
                <?php elseif ($model->name === 'home_page_id' ): ?>
                    <?php  echo $form->field($model, '[' . $i . ']value')->dropDownList(Settings::getPagesList())->label($model->description . " [ " . $model->name . " ] "); ?>
                <?php elseif($model->name === 'cms_page_on_top' || $model->name === 'cms_page_on_bottom') : ?>
                    <?php  echo $form->field($model, '[' . $i . ']value')->dropDownList(Settings::getPagesList(false))->label($model->description . " [ " . $model->name . " ] "); ?>
                <?php else : ?>
                    <?php echo $form->field($model, '[' . $i . ']value')->textInput(['maxlength' => 255])->label($model->description . " [ " . $model->name . " ] "); ?>
                <?php endif; ?>

            <?php endforeach ?>

            <?= $form->field($rules, 'value')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?php echo Html::submitButton(Yii::t('yii', 'Save'), ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        <?php else: ?>
            <?= Yii::t('yii','Site have no settings!') ?>
        <?php endif ?>

    </div>


</div>
