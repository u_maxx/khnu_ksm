<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ScientificActivities */

$this->title = Yii::t('yii', 'Create scientific activity');
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Lecturers: Scientific Activities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scientific-activities-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
