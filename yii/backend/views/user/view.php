<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use yii\grid\DataColumn;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->login;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this user?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'login',
            'email:email',
            [
                'class' =>  DataColumn::className(),
                'attribute' =>  'group',
                'value' =>  User::translatedGroupName($model->group),
            ],
            [
                'class' =>  DataColumn::className(),
                'attribute' =>  'status',
                'value' =>  User::translatedStatusName($model->status),
            ],
            [
                'attribute' =>  'is_teacher',
                'value' =>  ($model->is_teacher)? Yii::t('app', 'Yes') : Yii::t('app', 'No'),
            ],
            'created_at',
            'updated_at',
            'first_name',
            'second_name',
            'third_name',
            'about:ntext',
        ],
    ]) ?>

</div>
