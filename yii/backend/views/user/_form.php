<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $statuses[] */
/* @var $groups[] */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput() ?>
    <?= $form->field($model, 'email')->input('email') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <?php if(!$model->isNewRecord): ?>

        <?= $form->field($model, 'status')->dropDownList([
            0   =>  User::$statusesName[0],
            1   =>  User::$statusesName[1],
        ]) ?>

    <?php endif; ?>

    <?= $form->field($model, 'group')->dropDownList([
        0   =>  User::$rolesName[0],
        1   =>  User::$rolesName[1],
        2   =>  User::$rolesName[2],
    ]) ?>

    <?= $form->field($model, 'is_teacher')->dropDownList([
        0   =>  Yii::t('app', 'no'),
        1   =>  Yii::t('app', 'yes'),
    ]) ?>

    <?= $form->field($model, 'first_name')->textInput() ?>
    <?= $form->field($model, 'second_name')->textInput() ?>
    <?= $form->field($model, 'third_name')->textInput() ?>

    <?= $form->field($model, 'about')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
