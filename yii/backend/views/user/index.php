<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use common\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'login',
            'email:email',
            [
                'class' =>  DataColumn::className(),
                'attribute' =>  'group',
                'format' => 'raw',
                'content' => function ($model) {
                    return User::translatedGroupName($model->group);
                },
            ],
            [
                'class' =>  DataColumn::className(),
                'attribute' =>  'status',
                'format'    =>  'raw',
                'content' =>  function ($model){
                    return User::translatedStatusName($model->status);
                }
            ],
            [
                'attribute' =>  'is_teacher',
                'value' =>  function($model) {
                    return ($model->is_teacher) ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                },
            ],
            'first_name',
            'second_name',
            'third_name',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'  => '<span style="margin-right: 10px">{view}</span> <span style="margin-right: 10px">{update}</span> <span>{delete}</span>',
            ],
        ],
    ]); ?>

</div>
