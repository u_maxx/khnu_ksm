<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('yii', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php echo Html::a(Yii::t('yii', 'Create Page'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'title',

            [
                'attribute' =>  'is_published',
                'value' =>  function($model) {
                    return ($model->is_published) ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                },
            ],
            'slug',
            [
                'class'     => 'yii\grid\ActionColumn',
                'template'  => '<span style="margin-right: 10px">{view}</span> <span style="margin-right: 10px">{update}</span> <span>{delete}</span>',
            ],
        ],
    ]); ?>

</div>
