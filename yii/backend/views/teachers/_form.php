<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\PageAssets;
use \backend\assets\TinyMCEAssets;
use common\models\ScientificActivities;
use common\models\Subjects;
use common\models\Publications;
use dosamigos\multiselect\MultiSelect;

/* @var $this yii\web\View */
/* @var $model common\models\Teachers */
/* @var $form yii\widgets\ActiveForm */

TinyMCEAssets::register($this);
PageAssets::register($this);

$model->scActivitiesArray = $model->getArrayOfCurTeacherScActivities();
$model->publicationsArray = $model->getArrayOfCurTeacherPublications();
$model->subjectsArray = $model->getArrayOfCurTeacherSubjects();
?>

<div class="teachers-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'degree')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <div class="multiselect-cont">
    <?= Html::tag('label', Yii::t('yii', 'Publications')) ?>
    <?= MultiSelect::widget([
        'options' => ['multiple'=>'multiple'],
        'data' => Publications::getArrayOfPublications(),
        'attribute' => 'publicationsArray',
        'model' => $model,
        'clientOptions' =>
            [
                'numberDisplayed' => 1,
                'enableFiltering' => true,
                'maxHeight' => 400
            ],
    ]) ?>
    </div>

    <div class="multiselect-cont">
    <?= Html::tag('label', Yii::t('yii', 'Scientific activities')) ?>
    <?= MultiSelect::widget([
        'options' => ['multiple'=>'multiple'],
        'data' => ScientificActivities::getArrayOfScActivities(),
        'attribute' => 'scActivitiesArray',
        'model' => $model,
        'clientOptions' =>
            [
                'numberDisplayed' => 2,
                'enableFiltering' => true,
                'maxHeight' => 400
            ],
    ]) ?>
    </div>

    <div class="multiselect-cont">
    <?= Html::tag('label', Yii::t('yii', 'Subjects')) ?>
    <?= MultiSelect::widget([
        'options' => ['multiple'=>'multiple'],
        'data' => Subjects::getArrayOfSubjects(),
        'attribute' => 'subjectsArray',
        'model' => $model,
        'clientOptions' =>
            [
                'numberDisplayed' => 3,
                'enableFiltering' => true,
                'maxHeight' => 400
            ],
    ]) ?>
    </div>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?php if(!$model->isNewRecord)
        if($model->image)
            echo Html::img('/uploads/teachers-photos/' . $model->image, ['alt' => $model->name, 'class' => 'thumbnail', 'style' => 'max-width: 600px;']) ;
        else
            echo Html::tag('p', Yii::t('yii', 'There is no photo loaded for this lecturer.'), ['class' => 'bg-info', 'style' => 'padding: 20px 30px; font-style: italic;']);
    ?>

    <?= $form->field($model, 'about')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
