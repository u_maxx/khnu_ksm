<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 06.10.15
 * Time: 11:11
 */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<ul>
    <?php
    foreach($model->getArrayOfCurTeacherSubjects(true) as $subjectName){
        echo  Html::tag('li', HtmlPurifier::process($subjectName));
    }
    ?>
</ul>