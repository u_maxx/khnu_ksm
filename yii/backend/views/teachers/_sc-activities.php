<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 06.10.15
 * Time: 11:12
 */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<ul>
    <?php
    foreach($model->getArrayOfCurTeacherScActivities(true) as $scActivityName){
        echo  Html::tag('li', HtmlPurifier::process($scActivityName));
    }
    ?>
</ul>