<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $model common\models\Teachers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Lecturers: Biography'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teachers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('yii', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'degree',
            'email:email',
            [
                'attribute' =>  'image',
                'value' => ($model->image)? '/uploads/teachers-photos/' . $model->image  : Yii::t('yii', 'There is no photo loaded for this lecturer.'),
                'format' => ($model->image)? ['image',['class' => 'thumbnail', 'style'=>'max-width: 800px;', 'alt' => $model->name]] : 'text',
            ],
            [
                'attribute' =>  'scActivitiesArray',
                'format' => 'html',
                'value' => $this->render('_sc-activities',[
                    'model' => $model,
                ])
            ],
            [
                'attribute' =>  'subjectsArray',
                'format' => 'html',
                'value' => $this->render('_subjects',[
                    'model' => $model,
                ])
            ],
            [
                'attribute' =>  'publicationsArray',
                'format' => 'html',
                'value' => $this->render('_publications',[
                    'model' => $model,
                ])
            ],
            'about:html',

        ],
    ]) ?>

</div>
