<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 06.10.15
 * Time: 11:10
 */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<ul>
    <?php
    foreach($model->getArrayOfCurTeacherPublications(true) as $publicationName){
        echo  Html::tag('li', HtmlPurifier::process($publicationName));
    }
    ?>
</ul>
