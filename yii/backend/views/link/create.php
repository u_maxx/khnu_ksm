<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Link */

$this->title = Yii::t('yii', 'Create link');
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="link-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
