<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Slider'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="carousel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('yii', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            [
                'attribute' => 'link',
                'value' => ($model->link) ? Html::a(Yii::t('yii', 'Open link'), $model->link) : Yii::t('yii', 'There is no link selected.'),
                'format' => 'html'
            ],
            [
                'attribute' =>  'image',
                'value' => ($model->image)? '/uploads/carousel-images/' . $model->image  : Yii::t('yii', 'There is no image loaded for this slide.'),
                'format' => ($model->image)? ['image',['style'=>'max-width: 800px;', 'alt' => $model->title]] : 'text',
            ],
            [
                'attribute' => 'show_in_main',
                'value' => ($model->show_in_main) ? Yii::t('yii', 'Yes'): Yii::t('yii', 'No')
            ]
        ],
    ]) ?>

</div>
