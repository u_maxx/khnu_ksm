<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Articles;
use common\models\Page;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?php if(!$model->isNewRecord)
        if($model->image)
            echo Html::img('/uploads/carousel-images/' . $model->image, ['alt' => $model->title, 'class' => 'thumbnail', 'style' => 'max-width: 600px;']) ;
        else
            echo Html::tag('p', Yii::t('yii', 'There is no image loaded for this slide.'), ['class' => 'bg-info', 'style' => 'padding: 20px 30px; font-style: italic;']);
    ?>

    <?= $form->field($model, 'link')->dropDownList([Yii::t('yii','Pages') => Page::getArrayPagesLinks(), Yii::t('yii','Articles') => Articles::getArrayArticleLinks()], ['prompt'=> Yii::t('yii','Select link')]) ?>

    <?= $form->field($model, 'show_in_main')->dropDownList([0 => Yii::t('yii','No'), 1 => Yii::t('yii', 'Yes')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
