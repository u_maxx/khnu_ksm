<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('yii', 'Slider');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="carousel-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Create slide'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description',
            'image',
            [
                'attribute' => 'link',
                'value' => function($model) {
                    return ($model->link) ? Html::a(Yii::t('yii', 'Open link'), $model->link) : Yii::t('yii', 'There is no link selected.');
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'show_in_main',
                'value' => function($model) {
                    return ($model->show_in_main) ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
