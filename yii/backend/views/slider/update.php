<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = Yii::t('yii', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('yii', 'slide'),
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Slider'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('yii', 'Update');
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="carousel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
