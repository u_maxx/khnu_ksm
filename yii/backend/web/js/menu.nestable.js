$(function () {

    $("#save-menu").click(function () {
        if (confirm(("Are you sure?"))) {
            var menu = "menu=" + JSON.stringify($("#menu_items").nestable("serialize"));
            $.post('/admin/menu/save', menu, function (data) {
                location.reload();
            });
        }
    });

    $('#available_items').nestable({
        maxDepth: 1
    });

    $('#menu_items').nestable();

});