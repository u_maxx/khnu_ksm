<?php

return array(

	'Select' => 'Вибрати',
	'Erase' => 'Видалити',
	'Open' => 'Відкрити',
	'Confirm_del' => 'Впевнені, що хочете видалити цей файл?',
	'All' => 'Всі',
	'Files' => 'Файли',
	'Images' => 'Зображення',
	'Archives' => 'Архіви',
	'Error_Upload' => 'Файл, що завантажується перевищує дозволений розмір.',
	'Error_extension' => 'Неприпустимий формат файлу.',
	'Upload_file' => 'Завантажити файл',
	'Filters' => 'Фільтр',
	'Videos' => 'Відео',
	'Music' => 'Музика',
	'New_Folder' => 'Нова папка',
	'Folder_Created' => 'Папку успішно створено',
	'Existing_Folder' => 'Існуюча папка',
	'Confirm_Folder_del' => 'Впевнені, що хочете видалити цю папку і всі файли в ній?',
	'Return_Files_List' => 'Повернутися до списку файлів',
	'Preview' => 'Перегляд',
	'Download' => 'Завантажити',
	'Insert_Folder_Name' => 'Введіть ім`я папки:',
	'Root' => 'Коренева папка',
	'Rename' => 'Переіменувати',
	'Back' => 'назад',
	'View' => 'Вигляд',
	'View_list' => 'Список',
	'View_columns_list' => 'Стовпчики',
	'View_boxes' => 'Плиткою',
	'Toolbar' => 'Панель',
	'Actions' => 'Дії',
	'Rename_existing_file' => 'Файл вже існує',
	'Rename_existing_folder' => 'Папка вже існує',
	'Empty_name' => 'Не заповнено ім`я',
	'Text_filter' => 'фільтр',
	'Swipe_help' => 'Наведіть на ім`я файлу/папки, щоб побачити опції',
	'Upload_base' => 'Основне завантаження',
	'Upload_java' => 'JAVA-завантаження (для файлів великих розмірів)',
	'Upload_java_help' => "Якщо Java-апплет не завантажується: 1. переконайтесь, що Java встановлено на вашому комп`ютері, інакше <a href='http://java.com/en/download/'>[завантажте]</a> 2. переконайтесь, що фаєрвол нічого не блокує",
	'Upload_base_help' => "Перетягніть файли в область, що вище або клікніть по ній мишкою (для сучасних браузерів), в іншому разі виберіть файл та натисніть кнопку. Коли завантаження закінчиться - натисніть кнопку повернення.",
	'Type_dir' => 'папка',
	'Type' => 'Тип',
	'Dimension' => 'Розмір',
	'Size' => 'Об`єм',
	'Date' => 'Дата',
	'Filename' => 'Ім`я',
	'Operations' => 'Дії',
	'Date_type' => 'р-м-д',
	'OK' => 'OK',
	'Cancel' => 'Відміна',
	'Sorting' => 'Сортування',
	'Show_url' => 'показати URL',
	'Extract' => 'extract here',
	'File_info' => 'информація про файл',
	'Edit_image' => 'редагувати зображення',
	'Duplicate' => 'Дублювати',
	'Folders' => 'Теки',
	'Copy' => 'Копіювати',
	'Cut' => 'Вирізати',
	'Paste' => 'Вставити',
	'CB' => 'CB', // clipboard
	'Paste_Here' => 'Вставити в папку',
	'Paste_Confirm' => 'Are you sure you want to paste to this directory? This will overwrite existing files/folders if encountered any.',
	'Paste_Failed' => 'Помилка вставки файла(ів)',
	'Clear_Clipboard' => 'Очистити буфер',
	'Clear_Clipboard_Confirm' => 'Ви впевпені, що бажаєте очистити буфер?',
	'Files_ON_Clipboard' => 'Файлм в буфері.',
	'Copy_Cut_Size_Limit' => 'The selected files/folders are too big to %s. Limit: %d MB/operation', // %s = cut or copy
	'Copy_Cut_Count_Limit' => 'You selected too many files/folders to %s. Limit: %d files/operation', // %s = cut or copy
	'Copy_Cut_Not_Allowed' => 'You are not allowed to %s files.', // %s(1) = cut or copy, %s(2) = files or folders
	'Aviary_No_Save' => 'Зображення не забережено.',
	'Zip_No_Extract' => 'Could not extract. File might be corrupt.',
	'Zip_Invalid' => 'Розширення не підтримується. Підтримуються: zip, gz, tar.',
	'Dir_No_Write' => 'Вибрана папка не доступна для запису.',
	'Function_Disabled' => 'Функція %s відключена на сервері.', // %s = cut or copy
	'File_Permission' => 'Права',
	'File_Permission_Not_Allowed' => 'Зміна прав для %s не дозволена.', // %s = files or folders
	'File_Permission_Recursive' => 'Здійснити рекурсивно?',
	'File_Permission_Wrong_Mode' => "Неправильно задані права.",
	'User' => 'Користувач',
	'Group' => 'Група',
	'Yes' => 'Так',
	'No' => 'Ні',
	'Lang_Not_Found' => 'Мову не знайдено.',
	'Lang_Change' => 'Змінити мову',
	'File_Not_Found' => 'Could not find the file.',
	'File_Open_Edit_Not_Allowed' => 'Немає прав для %s цього файлу.', // %s = open or edit
	'Edit' => 'Редагувати',
	'Edit_File' => "Редагувати вміст файла",
	'File_Save_OK' => "Файл збережено.",
	'File_Save_Error' => "Помилка збереження файла.",
	'New_File' => 'Новий файл',
	'No_Extension' => 'Потрібно додати файл разширення.',
	'Valid_Extensions' => 'Направильне розширення: %s', // %s = txt,log etc.

);
