<?php
namespace backend\assets;

use yii\web\AssetBundle;

class MenuAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/menu.css'
    ];
    public $js = [
        'js/jquery.nestable.js',
        'js/menu.nestable.js',
        'js/scripts.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}