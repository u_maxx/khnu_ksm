<?php
/**
 * Created by PhpStorm.
 * User: veysman
 * Date: 22.06.15
 * Time: 17:40
 */

namespace backend\assets;

use yii\web\AssetBundle;

class TinyMCEAssets extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/tinymce/tinymce.min.js',
        'js/tinymce/tinymce-init.js',
        'js/scripts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
} 