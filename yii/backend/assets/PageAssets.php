<?php

namespace backend\assets;

use yii\web\AssetBundle;

class PageAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/parent-page-chooser.js',
        'js/scripts.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}