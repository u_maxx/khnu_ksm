<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\rbac\UserGroupRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); //удаляем старые данные

        //Включаем наш обработчик
        $rule = new UserGroupRule();
        $auth->add($rule);

        //Добавляем роли
        $user = $auth->createRole('user');
        $user->description = 'Користувач';
        $user->ruleName = $rule->name;
        $auth->add($user);

        $writer = $auth->createRole('writer');
        $writer->description = 'Блогер';
        $writer->ruleName = $rule->name;
        $auth->add($writer);

        $admin = $auth->createRole('admin');
        $admin->description = 'Адміністратор';
        $admin->ruleName = $rule->name;
        $auth->add($admin);


        //Добавить разрешения
        //$updatepost = $auth->createPermission('updatePost');
        //$updatepost->description = 'Update post';
        //$updatepost->ruleName = $rule->name;
        //$auth->add($updatepost);
        //$auth->addChild($user, $updatepost);


        //Раздать права
        $auth->addChild($writer, $user);
        $auth->addChild($admin, $writer);
    }
}