<?php

use yii\db\Schema;
use yii\db\Migration;

class m151024_194944_initialize_cms_pages extends Migration
{
    public function up()
    {
        $this->insert('pages',[
            'title'             =>  'Вітаємо на сайті кафедри КСМ ХНУ',
            'meta_description'  =>  'Кафедра комп\'ютерних систем (КСМ) була організована в 1989 році в складі факультету машинобудування та радіоелектроніки і мала назву “Кафедра електронно-обчислювальних систем”.',
            'meta_keyword'      =>  'КСМ, склад, заснована, ХНУ',
            'short_description' =>  '<div class="row"><div class="col-md-7">
                                        <p class="fullwidth-align jumbotron-paragraph">Кафедра комп\'ютерних систем та мереж заснована у 1991 році.</p>
                                        <p class="fullwidth-align jumbotron-paragraph">Кафедра входить до складу Інституту телекомунікаційних та комп\'ютерних систем Хмельницького національного університету. З червня 2005 року кафедру очолює доктор технічних наук, професор, академік міжнародної академії інформатизації М\'ясіщев О.А. На сьогоднішній день колектив кафедри стовітсотково складається з викладачів з науковими ступенями, які є висококваліфікованими спеціалістами в галузі проектування комп\'ютерних мереж, цифрових обчислювальних пристроїв та систем, мережних інформаційних технологій, захисту інформації. Лабораторна база кафедри є провідною в університеті і включає в себе більше ста одиниць сучасного комп\'ютерного та мережного устаткування.</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="thumbnail">
                                          <img class="img-responsive" src="/uploads/cms-images/ksm_team.jpg" alt="Викладацький склад кафедри КСМ">
                                        </div>
                                    </div></div>',
            'description'       =>  '<div class="row">
                                            <div style="float: right;" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <div class="thumbnail">
                                                    <img class="img-responsive" src="/uploads/cms-images/ksm_team.jpg" alt="Викладацький склад кафедри КСМ">
                                                </div>
                                            </div>
                                            <p>Кафедра комп\'ютерних систем (КСМ) була організована в 1989 році в складі факультету машинобудування та радіоелектроніки і мала назву “Кафедра електронно-обчислювальних систем”. На той час кафедру очолював кандидат технічних наук доцент Бардаченко В.Ф. Працівниками кафедри за сумісництвом працювали к.т.н. Графов Р.П. Та три асистенти. У 1991 році кафедра була включена в склад новоствореного факультету радіоелектроніки. В той час на кафедрі працювали два доценти (к.ф-м.н.), один доцент (к.т.н.) за сумісництвом та три асистенти. При кафедрі діяли одна науково-дослідна та чотири навчальних лабораторії.</p>

                                            <p>В жовтні 1992 року кафедру очолив к.т.н., старший науковий співробітник, доцент Локазюк В.М. Кафедра почала інтенсивно створювати навчальні лабораторії, вести підготовку спеціалістів вищої кваліфікації. Викладачі кафедри працювали над проблемами технічної діагностики обчислювальних пристроїв та систем при науково-дослідній лабораторії діагностування мікропроцесорних пристроїв, що розташовувались на дослідно-експериментальній базі інституту. На той час на кафедрі працювало два доценти (д.т.н.), два доценти (к.т.н) за сумісництвом, п\'ять асистентів. З 1995 року кафедру очолює д.т.н, проф. Локазюк В.М. До викладання курсів лекцій систематично залучались висококваліфіковані спеціалісти з інших університетів, зокрема, д.т.н., професор Байда М.П., д.т.н. Ротштейн О.П. Та к.т.н., доцент Перевознiков С.І. З Вінницького технічного університету. З 1996 року кафедра електронно-обчислювальних систем була перейменована на кафедру комп\'ютерних систем та мереж.</p>

                                            <p>З тих часів кафедрою пройдений великий шлях від початкового становлення і до ствердження її як випускаючого підрозділу з найвищими рейтинговими показниками. З червня 2005 року кафедру комп\'ютерних систем та мереж очолив д.т.н., професор, академік міжнародної академії інформатизації М\'ясіщев Олександр Анатолійович.</p>

                                            <p>На сьогоднішній день колектив кафедри стовідсотково складається з викладачів з науковими ступенями, активно проводить наукові дослідження та займається вдосконаленням організації навчального процесу. Щорічно кафедра комп\'ютерних систем та мереж займає призові місця в конкурсі випускаючих кафедр Хмельницького національного університету за якістю організації навчально-методичної, викладацької і наукової роботи. Високий рівень кваліфікації викладачів кафедри є передумовою забезпечення відповідного рівня підготовки студентів як майбутніх фахівців.</p>

                                            <p>Викладачі кафедри публікують щорічно 2-3 монографії (навчальні посібники), близько 10-15 наукових статей в фахових науково-технічних журналах, близько 30 тез доповідей в збірниках наукових конференцій, беруть активну участь в міжнародних та всеукраїнських науково-технічних та науково-практичних конференціях.</p>
                                                    <br>
                                                    <!-- 16:9 aspect ratio -->
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="thumbnail">
                                                            <div class="embed-responsive embed-responsive-16by9">
                                                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/nUdj6XgW5hA" frameborder="0" allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>

                                        </div>',
            'is_published'      =>  '1',
            'slug'              =>  'welcome-to-ksm',
        ]);
        $this->insert('pages',[
            'title'             =>  'Що таке комп\'ютерна інженерія?',
            'meta_description'  =>  'Комп’ютерна інженерія (Computer Engineering) – це напрям, який об’єднує в собі частини електротехніки, комп’ютерних наук та програмної інженерії',
            'meta_keyword'      =>  'комп\'ютер, інженерія, студенти, проектування, розроблення, програмування',
            'short_description' =>  '<div class="row"><div class="col-md-5">
                                        <img class="img-responsive" src="/uploads/cms-images/ki-no-bg.png" alt="Що таке комп\'ютерна інженерія">
                                    </div>
                                    <div class="col-md-7">
                                        <p class="fullwidth-align jumbotron-paragraph">До задач, які вирішують фахівці у галузі комп’ютерної інженерії відносять:</p>
                                        <ul class="jumbotron-paragraph">
                                            <li>розроблення прикладного та системного програмного забезпечення
                                            </li>
                                            <li>розроблення програмного забезпечення для вбудованих комп’ютерних систем;</li>
                                            <li>проектування мікропроцесорних пристроїв;</li>
                                            <li>проектування, налагодження та обслуговування комп’ютерних мереж;</li>
                                            <li>проектування операційних систем та інше.</li>
                                        </ul>
                                        <p class="fullwidth-align jumbotron-paragraph">Комп’ютерна інженерія (Computer Engineering) – це напрям, який об’єднує в собі частини електротехніки, комп’ютерних наук та програмної інженерії необхідні для проектування та розроблення комп’ютерних систем. Фахівці з комп’ютерної інженерії приймають участь у багатьох аспектах проектування апаратно-програмних систем, від проектування мікропроцесорів, персональних та супер комп’ютерів до проектування цифрових схем. Ця галузь інженерії зосереджена не лише на тому як функціонують комп’ютери самі по собі, але й включає усі аспекти інтеграції комп’ютерних систем у різні галузі, від технологічних процесів на виробництві, електростанціях та ін. до літальних космічних апаратів.</p>
                                    </div></div>',
            'description'       =>  '<div class="row">
                                            <div style="float: right;" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <img style="margin: 0 auto;" class="img-responsive" src="/uploads/cms-images/ki-no-bg.png" alt="Що таке комп\'ютерна інженерія">
                                            </div>
                                            <p><strong>Комп’ютерна інженерія (Computer Engineering)</strong> – це напрям, який об’єднує в собі частини електротехніки, комп’ютерних наук та програмної інженерії необхідні для проектування та розроблення комп’ютерних систем. Фахівці з комп’ютерної інженерії приймають участь у багатьох аспектах проектування апаратно-програмних систем, від проектування мікропроцесорів, персональних та супер комп’ютерів до проектування цифрових схем. Ця галузь інженерії зосереджена не лише на тому як функціонують комп’ютери самі по собі, але й включає усі аспекти інтеграції комп’ютерних систем у різні галузі, від технологічних процесів на виробництві, електростанціях та ін. до літальних космічних апаратів.</p>
                                        <h3>До задач, які вирішують фахівці у галузі комп’ютерної інженерії відносять:</h3>
                                        <ul class="jumbotron-paragraph">
                                            <li>розроблення прикладного та системного програмного забезпечення
                                            </li>
                                            <li>розроблення програмного забезпечення для вбудованих комп’ютерних систем;</li>
                                            <li>проектування мікропроцесорних пристроїв;</li>
                                            <li>проектування, налагодження та обслуговування комп’ютерних мереж;</li>
                                            <li>проектування операційних систем та інше.</li>

                                        </ul>
                                        Спеціалісти з комп’ютерної інженерії залучаються до проектних та дослідних робіт у галузі робототехніки, оскільки ця галузь вимагає використання цифрових пристроїв для моніторингу та керування такими компонентами, як електродвигуни, сенсори, система комунікації.</p>

                                        <p>Підчас навчання на факультеті комп’ютерних систем та програмування, студенти мають можливість обирати спеціалізацію (комп’ютерні мережі, програмування, системне програмування та ін.) для більш глибокого вивчення відповідних дисциплін.</p>

                                        <p>Після завершення 4-х річної програми підготовки бакалавра за напрямом комп’ютерна інженерія, студентам факультету комп’ютерних систем та програмування пропонується продовжити навчання за спеціальностями:</p>
                                    </div>',
            'is_published'      =>  '1',
            'slug'              =>  'what-does-it-mean-computer-engineering',
        ]);

        $this->insert('pages',[
            'title'             =>  'Історія кафедри КСМ',
            'meta_description'  =>  'Кафедра комп\'ютерних систем (КСМ) була організована в 1989 році в складі факультету машинобудування та радіоелектроніки і мала назву “Кафедра електронно-обчислювальних систем”.',
            'meta_keyword'      =>  'КСМ, заснована, ХНУ, Бардаченко В.Ф., Локазюк В.М., М\'ясіщев',
            'short_description' =>  '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="thumbnail">
                                            <img class="img-responsive" src="/uploads/cms-images/history_of_kafedra_2.jpg" alt="KHNU" >
                                        </div>
                                    </div>
                                    <p class="fullwidth-align jumbotron-paragraph">Хмельницький національний університет - найбільший на Поділлі вищий навчальний заклад, який готує спеціалістів з багатьох галузей знань і проводить навчальну, методичну, наукову та виховну роботу.</p>
                                    <p class="fullwidth-align jumbotron-paragraph">Університет був заснований у 1962 році. За час свого існування він пройшов шлях від загальнотехнічного факультету Українського поліграфічного інституту до Хмельницького національного університету, який має найвищий ІV рівень акредитації.</p>',
            'description'       =>  '<div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/history_of_kafedra_2.jpg" alt="KHNU" >
                                                    </div>
                                                </div>

                                                <p class="fullwidth-align jumbotron-paragraph">Хмельницький національний університет - найбільший на Поділлі вищий навчальний заклад, який готує спеціалістів з багатьох галузей знань і проводить навчальну, методичну, наукову та виховну роботу.</p>
                                                <p class="fullwidth-align jumbotron-paragraph">Університет був заснований у 1962 році. За час свого існування він пройшов шлях від загальнотехнічного факультету Українського поліграфічного інституту до Хмельницького національного університету, який має найвищий ІV рівень акредитації.</p>
                                                
                                                <p class="fullwidth-align jumbotron-paragraph">Підготовку майбутніх фахівців здійснюють 6 інститутів, до складу яких входять 14 факультетів, що забезпечують навчання за 17 напрямками освітньо-кваліфікаційної підготовки, в тому числі: економіка і підприємництво, менеджмент, педагогічна освіта, мистецтво, екологія, прикладна математика, комп\'ютерна інженерія, комп\'ютерні науки, міжнародні відносини, філологія, інженерна механіка, радіотехніка, електронні апарати.</p>

                                                <p class="fullwidth-align jumbotron-paragraph">Сьогодні в університеті за 38 спеціальностями навчається більше 14 тисяч студентів. Навчальний процес забезпечують 57 докторів наук, професорів, 340 кандидатів наук, доцентів.</p>

                                                 <p class="fullwidth-align jumbotron-paragraph">Сучасне суспільство неможливо уявити без використання комп’ютерів, а їх застосування в різних галузях господарської діяльності важко переоцінити. Тому підготовка спеціалістів з комп’ютерних систем та мереж є надзвичайно актуальною.</p>

                                                 <div style="float: right;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/history_of_kafedra_3.png" alt="Computer System and Networks kafedras logo">
                                                    </div>
                                                </div>

                                                 <p class="fullwidth-align jumbotron-paragraph">Кафедра комп\'ютерних систем (КСМ) була організована в 1989 році в складі факультету машинобудування та радіоелектроніки і мала назву “Кафедра електронно-обчислювальних систем”.</p>

                                                 <p class="fullwidth-align jumbotron-paragraph">На той час кафедру очолював кандидат технічних наук доцент Бардаченко В.Ф. Працівниками кафедри за сумісництвом працювали к.т.н. Графов Р.П. та три асистенти.</p>

                                                 <p class="fullwidth-align jumbotron-paragraph"> У 1991 році кафедра була включена в склад новоствореного факультету радіоелектроніки. В той час на кафедрі працювали два доценти (к.ф-м.н.), один доцент (к.т.н.) за сумісництвом та три асистенти. При кафедрі діяли одна науково-дослідна та чотири навчальних лабораторії.</p>

                                                 <p class="fullwidth-align jumbotron-paragraph">В жовтні 1992 року кафедру очолив к.т.н., старший науковий співробітник, доцент Локазюк В.М. Кафедра почала інтенсивно створювати навчальні лабораторії, вести підготовку спеціалістів вищої кваліфікації. Викладачі кафедри працювали над проблемами технічної діагностики обчислювальних пристроїв та систем при науково-дослідній лабораторії діагностування мікропроцесорних пристроїв, що розташовувались на дослідно-експериментальній базі інституту. На той час на кафедрі працювало два доценти (д.т.н.), два доценти (к.т.н) за сумісництвом, п\'ять асистентів. З 1995 року кафедру очолює д.т.н, проф. Локазюк В.М.</p>

                                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/ksm_team.jpg" alt="Computer Systems and Networks lecturers">
                                                    </div>
                                                </div>  

                                                 <p class="fullwidth-align jumbotron-paragraph">До викладання курсів лекцій систематично залучались висококваліфіковані спеціалісти з інших університетів, зокрема, д.т.н., професор Байда М.П., д.т.н. Ротштейн О.П. та к.т.н., доцент Перевознiков С.І. з Вінницького технічного університету.</p>

                                                 <p class="fullwidth-align jumbotron-paragraph"> З 1996 року кафедра електронно-обчислювальних систем була перейменована на кафедру комп\'ютерних систем та мереж.</p>

                                                 <p class="fullwidth-align jumbotron-paragraph">З тих часів кафедрою пройдений великий шлях від початкового становлення і до ствердження її як випускаючого підрозділу з найвищими рейтинговими показниками. З червня 2005 року кафедру комп\'ютерних систем та мереж очолив д.т.н., професор, академік міжнародної академії інформатизації М\'ясіщев Олександр Анатолійович.</p>

                                                 <p class="fullwidth-align jumbotron-paragraph">На сьогоднішній день колектив кафедри стовідсотково складається з викладачів з науковими ступенями, активно проводить наукові дослідження та займається вдосконаленням організації навчального процесу. Щорічно кафедра комп\'ютерних систем та мереж займає призові місця в конкурсі випускаючих кафедр Хмельницького національного університету за якість організації навчально-методичної, викладацької і наукової роботи. Високий рівень кваліфікації викладачів кафедри є передумовою забезпечення відповідного рівня підготовки студентів як майбутніх фахівців.</p>

                                                 <p class="fullwidth-align jumbotron-paragraph">Викладачі кафедри публікують щорічно 2-3 монографії (навчальні посібники), близько 10-15 наукових статей в фахових науково-технічних журналах, близько 30 тез доповідей в збірниках наукових конференцій, беруть активну участь в міжнародних та всеукраїнських науково-технічних та науково-практичних конференціях.</p>

                                                <div class="col-lg-offset-3 col-md-offset-3 col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div id="carousel-charter" class="carousel slide" data-ride="carousel" >
                                                        <!-- Indicators -->
                                                        <ol class="carousel-indicators">
                                                            <li data-target="#carousel-charter" data-slide-to="0" class="active"></li>
                                                            <li data-target="#carousel-charter" data-slide-to="1"></li>
                                                            <li data-target="#carousel-charter" data-slide-to="2"></li>
                                                            <li data-target="#carousel-charter" data-slide-to="3"></li>
                                                            <li data-target="#carousel-charter" data-slide-to="4"></li>
                                                        </ol>

                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            <div class="item active">
                                                                <img src="/uploads/cms-images/001_b.jpg" alt="Awards">                           
                                                            </div>
                                                            <div class="item">
                                                                <img src="/uploads/cms-images/002_b.jpg" alt="Awards">                           
                                                            </div>
                                                            <div class="item">
                                                                <img src="/uploads/cms-images/003_b.jpg" alt="Awards">                           
                                                            </div>
                                                            <div class="item">
                                                                <img src="/uploads/cms-images/004_b.jpg" alt="Awards">                           
                                                            </div>
                                                            <div class="item">
                                                                <img src="/uploads/cms-images/005_b.jpg" alt="Awards">                           
                                                            </div>
                                                        </div>

                                                        <!-- Controls -->
                                                        <a class="left carousel-control" href="#carousel-charter" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                        </a>
                                                        <a class="right carousel-control" href="#carousel-charter" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                        </a>
                                                    </div> 
                                                </div>                    
                                                
                                            </div>
                                            
                                            <div class="row">
                                                <p class="fullwidth-align jumbotron-paragraph">На базі кафедри комп\'ютерних систем та мереж створені комп’ютерні класи, які підключені до мережі Internet, лабораторії мультимедійних засобів, САПР, мікропроцесорних систем, локальних та обчислювальних мереж.</p>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_1.jpg" alt="Students in computer class perform laboratory work">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_2.jpg" alt="Students in computer class perform laboratory work">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_3.jpg" alt="Students in computer class perform laboratory work">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_4.jpg" alt="Students in Information and Computer Center perform laboratory work">
                                                    </div>
                                                </div> 

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_5.jpg" alt="Students in Information and Computer Center perform laboratory work">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_6.jpg" alt="Server in information and computer center">
                                                    </div>
                                                </div> 

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_8.jpg" alt="Server in information and computer center">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_9.jpg" alt="Server in information and computer center">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_10.jpg" alt="Server in information and computer center">
                                                    </div>
                                                </div>   

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/210_7.jpg" alt="Lecture halls">
                                                    </div>
                                                </div>
                                            </div>',
            'is_published'      =>  '1',
            'slug'              =>  'history-of-kafedra',
        ]);
        $this->insert('pages',[
            'title'             =>  'Навчальні плани',
            'meta_description'  =>  'Навчальні плани на освітньо-кваліфікаційний рівень - спеціаліст',
            'meta_keyword'      =>  'плани, спеціаліст, магістр, рівень',
            'short_description' =>  '<h2 align="center">Навчальні плани на освітньо-кваліфікаційний рівень - <strong>спеціаліст</strong></h2>
                                    <br>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="thumbnail">
                                            <img class="img-responsive" src="/uploads/cms-images/plans_1.jpg" alt="Study plans for education and qualification level - specialist">
                                        </div>
                                    </div>',
            'description'       =>  '<div class="row">
                                                <h2 align="center">Навчальні плани на освітньо-кваліфікаційний рівень - <strong>спеціаліст</strong></h2>
                                                <br>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/plans_1.jpg" alt="Study plans for education and qualification level - specialist">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/plans_2.jpg" alt="Study plans for education and qualification level - specialist">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/plans_3.jpg" alt="Study plans for education and qualification level - specialist">
                                                    </div>
                                                </div>
                                                <br>
                                                <h2 align="center">Навчальні плани на освітньо-кваліфікаційний рівень - <strong>магістр</strong></h2>
                                                <br>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/plans_4.jpg" alt="Curricula for education level - Master">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/plans_5.jpg" alt="Curricula for education level - Master">
                                                    </div>
                                                </div>

                                                <div class="" lass="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/plans_6.jpg" alt="Curricula for education level - Master">
                                                    </div>
                                                </div>
                                               
                                            </div>',
            'is_published'      =>  '1',
            'slug'              =>  'curricula',
        ]);
        $this->insert('pages',[
            'title'             =>  'Наукова діяльність',
            'meta_description'  =>  'Напрями наукової діяльності кафедри комп\'ютерних систем та мереж',
            'meta_keyword'      =>  'напрями, розробка, інформація, дані, мережа',
            'short_description' =>  '<div class="alert-own alert-success" role="alert">
                                        <p align="left" class="fullwidth-align jumbotron-paragraph"><strong>Напрями наукової діяльності кафедри комп\'ютерних систем та мереж:</strong></p>
                                        <br>
                                        <ul>
                                            <li>Проектування та розробка цифрових засобів контролю та керування технологічним процесами та відповідного програмного забезпечення.</li>
                                            <li>Розробка та супроводження автоматизованих систем з використанням сучасних систем керування базами даних та візуальних інструментальних засобів (основне та допоміжне виробництво).</li>
                                            <li>Розробка Web - сайтів (реклама).</li>
                                            <li>Розробка цифрових відеороликів.</li>
                                            <li>Розробка наочних графічних матеріалів з використанням сучасних графічних пакетів.</li>
                                            <li>Проведення навчальних курсів по підвищенню кваліфікації в області комп\'ютерних технологій.</li>
                                            <li>Розробка та супроводження контрольно-навчаючих програм, в тому числі по вивченню нормативних баз, технологічних процесів та ін.</li>
                                            <li>Розробка та супроводження систем підтримки та прийняття рішень.</li>
                                            <li>Аналіз, прогнозування, автоматизація господарської діяльності підприємства.</li>
                                            <li>Розробка, супроводження та розміщення в Internet баз даних (Internet -магазини і т.п)</li>
                                            <li>Моделювання, модернізація, оптимізація, аналіз вузьких місць комп\'ютерних мереж.</li>
                                            <li>Захист інформацій. Розробка методики захисту. Шифрування інформації. Захист інформації в базах даних, комп\'ютерних мережах.</li>
                                            <li>Перенесення інформації на електронні носії, надання засобів пошуку та візуалізації необхідної інформації.</li>
                                            <li>Розробка програмно-апаратних засобів діагностування.</li>
                                        </ul>
                                    </div>',
            'description'       =>  '<div class="row">
                                                <div class="alert-own alert-success" role="alert">
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph"><strong>Напрями наукової діяльності кафедри комп\'ютерних систем та мереж:</strong></p>
                                                    <br>
                                                    <ul>
                                                        <li>Проектування та розробка цифрових засобів контролю та керування технологічним процесами та відповідного програмного забезпечення.</li>
                                                        <li>Розробка та супроводження автоматизованих систем з використанням сучасних систем керування базами даних та візуальних інструментальних засобів (основне та допоміжне виробництво).</li>
                                                        <li>Розробка Web - сайтів (реклама).</li>
                                                        <li>Розробка цифрових відеороликів.</li>
                                                        <li>Розробка наочних графічних матеріалів з використанням сучасних графічних пакетів.</li>
                                                        <li>Проведення навчальних курсів по підвищенню кваліфікації в області комп\'ютерних технологій.</li>
                                                        <li>Розробка та супроводження контрольно-навчаючих програм, в тому числі по вивченню нормативних баз, технологічних процесів та ін.</li>
                                                        <li>Розробка та супроводження систем підтримки та прийняття рішень.</li>
                                                        <li>Аналіз, прогнозування, автоматизація господарської діяльності підприємства.</li>
                                                        <li>Розробка, супроводження та розміщення в Internet баз даних (Internet -магазини і т.п)</li>
                                                        <li>Моделювання, модернізація, оптимізація, аналіз вузьких місць комп\'ютерних мереж.</li>
                                                        <li>Захист інформацій. Розробка методики захисту. Шифрування інформації. Захист інформації в базах даних, комп\'ютерних мережах.</li>
                                                        <li>Перенесення інформації на електронні носії, надання засобів пошуку та візуалізації необхідної інформації.</li>
                                                        <li>Розробка програмно-апаратних засобів діагностування.</li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_1.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_2.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_3.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_4.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_5.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_6.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_7.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_8.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_9.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_10.jpg" alt="Scientific activities teachers of Computer Systems and Networks cafedras">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_11.jpg" alt="Research work of students">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/scientific activity_12.jpg" alt="">
                                                    </div>
                                                </div>
                                               </div>',
            'is_published'      =>  '1',
            'slug'              =>  'scientific-activity',
        ]);
        $this->insert('pages',[
            'title'             =>  'Дозвілля студентів',
            'meta_description'  =>  'Студенти університету активні не тільки у навчальному процесі, а й у спорті, громадській діяльності, музиці, танцях та інших заняттях, притаманних молоді.',
            'meta_keyword'      =>  'студенти, спорт, музика, здібності, визнання',
            'short_description' =>  '<p class="fullwidth-align jumbotron-paragraph">Студенти університету активні не тільки у навчальному процесі, а й у спорті, громадській діяльності, музиці, танцях та інших заняттях, притаманних молоді. На фото зображені студенти, які продемонстрували свої здібності у різних видах занять. Багато з них отримали світове визнання та можливість подорожувати по світу.</p>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="thumbnail">
                                            <img class="img-responsive" src="/uploads/cms-images/student_leisure_1.jpg" alt="European competition in the deadlift">
                                        </div>
                                    </div>',
            'description'       =>  '<div class="row">
                                                <h2 align="center">Студентське дозвілля</h2>
                                                <br>
                                                <p class="fullwidth-align jumbotron-paragraph">Студенти університету активні не тільки у навчальному процесі, а й у спорті, громадській діяльності, музиці, танцях та інших заняттях, притаманних молоді. На фото зображені студенти, які продемонстрували свої здібності у різних видах занять. Багато з них отримали світове визнання та можливість подорожувати по світу.</p>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_1.jpg" alt="European competition in the deadlift">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_2.jpg" alt="European competition in the deadlift">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_3.jpg" alt="European competition in the deadlift">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_4.jpg" alt="Canoes racing">
                                                    </div>
                                                </div>

                                                <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_6.jpg" alt="Canoes racing">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_5.jpg" alt="Jumping into the water">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_7.jpg" alt="Billiards">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_9.jpg" alt="Rock band">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_10.jpg" alt="Professional dance group">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_11.jpg" alt="Holidays in the mountains">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_12.jpg" alt="Holidays in the mountains">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_13.png" alt="Kick boxing">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_14.png" alt="Kick boxing">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_8.jpg" alt="Action to support the unity of Ukraine">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_15.jpg" alt="Action against the war in eastern Ukraine">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_16.jpg" alt="Action against the war in eastern Ukraine">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_17.jpg" alt="Students celebrate Christmas">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_18.jpg" alt="Rest in the Forest">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="/uploads/cms-images/student_leisure_19.jpg" alt="Rest in the Forest">
                                                    </div>
                                                </div>
                                            </div>',
            'is_published'      =>  '1',
            'slug'              =>  'students-leisure',
        ]);
        $this->insert('pages',[
            'title'             =>  'Характеристика спеціальності',
            'meta_description'  =>  'Навчальний процес за спеціальністю проводиться кафедрою "Комп\'ютерних систем та мереж".',
            'meta_keyword'      =>  'розробка, мережа, системи, кафедра, студенти, комп’ютер',
            'short_description' =>  '<h2 align="center">Спеціальність Комп’ютерні системи та мережі</h2>
                                    <br>
                                    <div style="float: right;" class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                        <div class="thumbnail">
                                            <img  class="img-responsive" src="/uploads/cms-images/сharacteristic_specialty_1.jpg" alt="Conferention">
                                        </div>
                                    </div>
                                    <p class="fullwidth-align jumbotron-paragraph">Навчальний процес за спеціальністю проводиться кафедрою "Комп\'ютерних систем та мереж". Кафедра комп’ютерних систем та мереж заснована у 1991 році. Кафедра входить до складу факультету програмування та комп’ютерних і телекомунікаційних систем Хмельницького національного університету. З червня 2005 року кафедру очолює доктор технічних наук, професор, академік міжнародної академії інформації М’ясіщев Олександр Анатолійович. На сьогоднішній день колектив кафедри стовідсотково складається з викладачів з науковими ступенями, які є висококваліфікованими спеціалістами в галузі проектування комп’ютерних мереж, цифрових обчислювальних пристроїв та систем, мережних інформаційних технологій, захисту інформації.</p>',
            'description'       =>  '<div class="row">
                                                <h2 align="center">ІНСТИТУТ ТЕЛЕКОМУНІКАЦІЙНИХ ТА КОМП\'ЮТЕРНИХ СИСТЕМ</h2>
                                                <h3 align="center">Факультет: Програмування та комп’ютерних і телекомунікаційних систем</h3>
                                                <h3 align="center">Напрям підготовки: Комп’ютерна інженерія (Computer engineering)</h3>
                                                <div class="alert-own alert-info" role="alert">
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Комп\'ютерна інженерія (Computer Engineering) - це напрям, який об\'єднує в собі частини електротехніки, комп\'ютерних наук та програмної інженерії необхідні для проектування та розроблення комп\'ютерних систем. Спеціалісти у галузі комп\'ютерної інженерії мають знання з електротехніки, технологій проектування програмних систем та програмно-апаратних комплексів, на відміну від інших комп\'ютерних напрямів, які зосереджуються лише на одній галузі знань.</p>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">До задач, які вирішують фахівці у галузі комп\'ютерної інженерії відносять:</p>
                                                    <ul>
                                                        <b>
                                                            <li>розроблення прикладного та системного програмного забезпечення;</li>
                                                            <li>розроблення програмного забезпечення для вбудованих комп\'ютерних систем;</li>
                                                            <li>проектування мікропроцесорних та мікроконтролерних пристроїв;</li>
                                                            <li>розроблення аналогових та гібридних плат;</li>
                                                            <li>проведення досліджень для робототехніки — синтезу систем керування двигунами, датчиками та іншим устаткуванням;</li>
                                                            <li>проектування, налагодження та обслуговування комп\'ютерних мереж;</li>
                                                            <li>розробка операційних систем та інших системних програм.</li>
                                                        </b>
                                                    </ul>
                                                    <br>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Після завершення 4-х річної програми підготовки бакалавра за напрямом комп\'ютерна інженерія, студентам факультету пропонується  продовжити навчання за спеціальностями:</p>
                                                    <div class="row">
                                                    <div class="col-md-offset-2 col-lg-offset-2 col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                        <div class="thumbnail">
                                                            <img class="img-responsive" src="/uploads/cms-images/example1.jpg" alt="Computer engineering">
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Напрями наукової діяльності кафедри комп\'ютерних систем та мереж:</p>
                                                    <ul>
                                                        <b>
                                                            <li>Комп\'ютерні системи та мережі;</li>
                                                            <li>Системне програмування;</li>
                                                            <li>В липні 2016 року планується набір на нову спеціальність "КІБЕРБЕЗПЕКА".</li>
                                                        </b>
                                                    </ul>
                                                </div>
                                                <div class="alert-own alert-warning" role="alert">
                                                    <h2 align="center">Спеціальність Комп’ютерні системи та мережі</h2>
                                                    <br>
                                                    <div style="float: right;" class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                                        <div class="thumbnail">
                                                            <img  class="img-responsive" src="/uploads/cms-images/сharacteristic_specialty_1.jpg" alt="Conferention">
                                                        </div>
                                                    </div>
                                                    <p class="fullwidth-align jumbotron-paragraph">Навчальний процес за спеціальністю проводиться кафедрою "Комп\'ютерних систем та мереж". Кафедра комп’ютерних систем та мереж заснована у 1991 році. Кафедра входить до складу факультету програмування та комп’ютерних і телекомунікаційних систем Хмельницького національного університету. З червня 2005 року кафедру очолює доктор технічних наук, професор, академік міжнародної академії інформації М’ясіщев Олександр Анатолійович. На сьогоднішній день колектив кафедри стовідсотково складається з викладачів з науковими ступенями, які є висококваліфікованими спеціалістами в галузі проектування комп’ютерних мереж, цифрових обчислювальних пристроїв та систем, мережних інформаційних технологій, захисту інформації.</p>
                                                    <div style="float: left;" class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                                        <div class="thumbnail">
                                                            <img class="img-responsive" src="/uploads/cms-images/210_6.jpg" alt="Server in information and computer center">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <p class="fullwidth-align jumbotron-paragraph">Лабораторна база кафедри є провідною в університеті і включає в себе більше ста одиниць сучасного комп’ютерного і мережного устаткування. Викладачі кафедри публікують кожні 5 років 2-3 монографії(навчальні посібники), щорічно близько 10-15 наукових статей в фахових науково-технічних журналах, близько 30 тез доповідей в збірниках наукових конференцій, беруть активну участь в міжнародних та всеукраїнських науково-технічних та науково-практичних конференціях.</p>
                                                    <p class="fullwidth-align jumbotron-paragraph">На базі кафедри комп’ютерних систем та мереж створені комп’ютерні класи, які підключені до мережі Internet, лабораторії мультимедійних засобів, САПР, мікропроцесорних систем, локальних та обчислювальних мереж.</p>
                                                    <p class="fullwidth-align jumbotron-paragraph">Випускники кафедри працюють та займають керівні посади в Україні та за кордоном у провідних фірмах, що займаються розробкою та впровадженням комп’ютерних систем штучного інтелекту, проектуванням спеціалізованих процесорів, побудовою високопродуктивних обчислювальних систем та мереж.</p><br>
                                                     <p align="left" class="fullwidth-align jumbotron-paragraph">Студенти можуть здобути наступні освітньо-кваліфікаційні рівні:</p>
                                                    <ul>
                                                        <li><b>Бакалавр</b>, кваліфікація фахівця - бакалавр комп\'ютерної інженерії (4 роки навчання);</li>
                                                        <li><b>Спеціаліст</b>, на базі освітньо-кваліфікаційного рівня бакалавра, кваліфікація фахівця - інженер системотехнік (1 рік навчання);</li>
                                                        <li><b>Магістр</b>, на базі освітньо-кваліфікаційного рівня бакалавра, кваліфікація фахівця - магістр комп\'ютерної інженерії (1 рік навчання).</li>
                                                    </ul>
                                                    <br>
                                                    
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">В процесі навчання студенти опановують більше сорока спеціальних дисциплін, що стосуються локальних та глобальних комп\'ютерних мереж (Internet, Ethernet, Token Ring, ATM та ін.), операційних систем (Windows Vista, Windows XP, Windows 2000 Server, Unix, Linux, Novell Netware), мов програмування (C++, Assembler, Perl, PHP, XML, Java, Delphi, та ін.), систем керування базами даних (FoxPRO, SQL Server, InterBase, Oracle, MS Access), основ комп\'ютерного моделювання та штучного інтелекту (Matlab 7.0, NeuroSolutions та ін.).</p>

                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Перелік основних дисциплін підготовки магістрів та спеціалістів:</p>
                                                    <ul>
                                                        <b>
                                                            <li>теорія проектування комп\'ютерних систем та мереж;</li>
                                                            <li>мережні інформаційні технології;</li>
                                                            <li>комп\'ютерні системи штучного інтелекту;</li>
                                                            <li>комп\'ютерні телекомунікаційні системи;</li>
                                                            <li>сервіси комп\'ютерної мережі Інтернет.</li>
                                                        </b>
                                                    </ul>
                                                    <br>

                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Напрями наукової діяльності кафедри комп’ютерних систем та мереж:</p>
                                                    <ul>
                                                        <b>
                                                            <li>Проектування та розробка цифрових засобів контролю та керування технологічними процесами та відповідного програмного забезпечення.</li>
                                                            <li>Розробка та супроводження автоматизованих систем з використанням сучасних систем керування базами даних та візуальних інструментальних засобів (основне та допоміжне виробництво).</li>
                                                            <li>Розробка Web-сайтів (реклама).</li>
                                                            <li>Розробка цифрових Відеороликів.</li>
                                                            <li>Розробка наочних графічних матеріалів з використанням сучасних графічних пакетів.</li>
                                                            <li>Проведення навчальних курсів по підвищенню кваліфікації в області комп\'ютерних технологій.</li>
                                                            <li>Розробка та супроводження контрольно-навчаючих програм, в тому числі по вивченню нормативних баз, технологічних процесів та ін.</li>
                                                            <li>Розробка та супроводження систем підтримки та прийняття рішень.</li>
                                                            <li>Аналіз, прогнозування, автоматизація господарської діяльності підприємства.</li>
                                                            <li>Розробка, супроводження та розмішення в Internet баз даних (Internet -магазини і т.п.).</li>
                                                        </b>
                                                    </ul>
                                                    <br>
                                                    <!-- 16:9 aspect ratio -->
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="thumbnail">
                                                            <div class="embed-responsive embed-responsive-16by9">
                                                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/nUdj6XgW5hA" frameborder="0" allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Сфера діяльності випускника спеціальності "Комп\'ютерні системи та мережі":</p>
                                                    <ul>
                                                        <b>
                                                            <li>розробка, встановлення та налагодження комп\'ютерних систем та мереж;</li>
                                                            <li>встановлення та супровід програмного забезпечення комп\'ютерних мереж;</li>
                                                            <li>розробка навігаційного та пошукового програмного забезпечення мережі Інтернет;</li>
                                                            <li>розробка та проектування мікропроцесорних та комп\'ютерних пристроїв різноманітного призначення;</li>
                                                            <li>розробка засобів технічного діагностування комп\'ютерних систем та мереж;</li>
                                                            <li>дослідницька та наукова діяльність.</li>
                                                        </b>
                                                    </ul>
                                                    <br>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Посади, які можуть займати випускники:</p>
                                                    <ul>
                                                        <b>
                                                            <li>інженер системотехнік;</li>
                                                            <li>оператор електронно-обчислювальних машин;</li>
                                                            <li>адміністратор комп\'ютерних мереж;</li>
                                                            <li>спеціаліст з проектування комп\'ютерних систем та мереж;</li>
                                                            <li>інженер з обслуговування комп\'ютерних систем та мереж.</li>
                                                        </b>
                                                    </ul>
                                                    <br>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Приклади вакансій доступних нашим випускникам(*):</p>
                                                    <ul>
                                                        <b>
                                                            <li>Спеціаліст тех.підтримки, Unix Administrator, заробітна плата 10 000 грн;</li>
                                                            <li>Адміністратор 1С, заробітна плата від 10 000 грн ;</li>
                                                            <li>Майстер-інженер по ремонту техніки "Apple", від 15 000 грн (вакансія доступна в м.Хмельницький);</li>
                                                            <li>Сервісний інженер, системний адміністратор, від 4 500 грн (вакансія доступна в м.Хмельницький);</li>
                                                            <li>PHP-програміст, від 10 000 грн (вакансія доступна в м.Хмельницький);</li>
                                                            <li>Веб-верстальщик, від 8 000 грн (вакансія доступна в м.Хмельницький);</li>
                                                            <li>Програміст мікроконтролерних систем, від 10 000 грн;</li>
                                                            <li>Розробник додатків для Android, від 10 000 грн;</li>
                                                            <li>Монтажник локальних мереж, від 5 000 грн (вакансія доступна в м.Хмельницький).</li>
                                                        </b>
                                                    </ul>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph"><small>*дані взяті з ресурсів rabota.ua, work.ua, jobs.ua</small></p>
                                                    <br>
                                                    <!-- 16:9 aspect ratio -->
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="thumbnail">
                                                            <div class="embed-responsive embed-responsive-16by9">
                                                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/6QmNtRTBkqk" frameborder="0" allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Основні вимоги до спеціалістів:</p>
                                                    <ul>
                                                        <b>
                                                            <li>Вища освіта;</li>
                                                            <li>Знання мов програмування С, С++, C#, Java (відповідно до вакансії);</li>
                                                            <li>Знання веб-технологій (відповідно до вакансії);</li>
                                                            <li>Знання схемотехніки та архітектури мікроконтролера;</li>
                                                            <li>Знання операційних систем Windows, Linux, Unix Free BSD;</li>
                                                            <li>Знання мережевих протоколів та технологій;</li>
                                                        </b>
                                                    </ul>
                                                    <br>
                                                    <p class="fullwidth-align jumbotron-paragraph">При кафедрі існує аспірантура (бюджетна та контрактна форма). Випусники захищають наукові і дисертаційні роботи з присвоєнням вченого звання «кандидат технічних наук» в спеціалізованій вченій раді університету.</p>
                                                    <div class="row">
                                                        <div class="col-xs-offset-1 col-sm-offset-2 col-xs-6 col-sm-6 col-md-8 col-lg-8">
                                                            <table class="table table-bordered vert-al">
                                                            <caption class="text-center"><h3><i>Кількість місць</i></h3></caption>
                                                                <tr class="active ">
                                                                    <th  rowspan="2">
                                                                        Спеціальності бакалаврів
                                                                    </th>
                                                                    <th colspan="2">
                                                                        Ліцезійний обсяг
                                                                    </th>
                                                                    <th rowspan="2">
                                                                        Спеціальності магістрів
                                                                    </th>
                                                                    <th>
                                                                        Ліцензійний обсяг
                                                                    </th>
                                                                </tr>
                                                                <tr class="active">
                                                                    <td>
                                                                        денна
                                                                    </td>
                                                                    <td>
                                                                        заочна
                                                                    </td>
                                                                    <td>
                                                                        денна
                                                                    </td>
                                                                </tr>
                                                                <tr class="active">
                                                                    <td rowspan="2">
                                                                        Комп’ютерна інженерія
                                                                    </td>
                                                                    <td rowspan="2">
                                                                        110
                                                                    </td>
                                                                    <td rowspan="2">
                                                                        30
                                                                    </td>
                                                                    <td>
                                                                        Комп’ютерні системи та мережі
                                                                    </td>
                                                                    <td>
                                                                        35
                                                                    </td>
                                                                </tr>
                                                                <tr class="active">
                                                                    <td>
                                                                        Системне програмування
                                                                    </td>
                                                                    <td>
                                                                        25
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>',
            'is_published'      =>  '1',
            'slug'              =>  'characteristic-speciality',
        ]);
        $this->insert('pages',[
            'title'             =>  'Болонська система',
            'meta_description'  =>  'У рамках Болонської угоди країнами-учасницями передбачається здійснити трохи важливих кроків по зближенню своїх освітніх систем.',
            'meta_keyword'      =>  'процес, угода, градації, система, Болонська, студенти',
            'short_description' =>  '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                        <div class="thumbnail">
                                            <img  class="img-responsive" src="/uploads/cms-images/bologna_system_1.jpg" alt="National Bologna Center logo">
                                        </div>
                                    </div>
                                    <p align="left" class="fullwidth-align jumbotron-paragraph">У рамках <b>Болонської угоди</b> країнами-учасницями передбачається здійснити трохи важливих кроків по зближенню своїх освітніх систем. Насамперед, прийняття загальної двухциклевої системи вищої школи. Випускник першого циклу, тривалістю не менш трьох років, одержує ступінь бакалавра. Другий цикл (тривалістю один-два роки) готує магістрів. При цьому і бакалавр, і магістр — ступені вищого утворення. Різниця між ними, полягає в тім, що бакалавр орієнтований на практичну діяльність, а магістр — на наукову і викладацьку роботу.</p>
                                    <p align="left" class="fullwidth-align jumbotron-paragraph"> Важливою віхою Болонского процесу є впровадження обліку обсягу знань у спеціальних одиницях, називаних кредитами. Зміст <b>європейської кредитно-трансферної системи (ECTS)</b> легко пояснити на конкретному прикладі. Якщо студент прослухав і успішно здав курс математичного аналізу в одному університеті, то у випадку переходу в іншій йому не треба слухати і здавати курс заново: на новому місці навчання йому зарахують зароблені кредити. І навіть якщо в новому вузі відповідний курс в обраній спеціальності буде називатися вже не «математичний аналіз», а якось інакше, його не потрібно буде перездавати. Таким чином, система кредитів — інструмент уніфікації вимог до навчання в різних університетах, що дозволяє студентові без праці переміщатися з одного вузу в іншій, з однієї держави в інше.</p>',
            'description'       =>  '<div class="row">
                                                <h2 align="center">Болонська система</h2>
                                                <br>
                                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                                        <div class="thumbnail">
                                                            <img  class="img-responsive" src="/uploads/cms-images/bologna_system_1.jpg" alt="National Bologna Center logo">
                                                        </div>
                                                </div>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">У рамках <b>Болонської угоди</b> країнами-учасницями передбачається здійснити трохи важливих кроків по зближенню своїх освітніх систем. Насамперед, прийняття загальної двухциклевої системи вищої школи. Випускник першого циклу, тривалістю не менш трьох років, одержує ступінь бакалавра. Другий цикл (тривалістю один-два роки) готує магістрів. При цьому і бакалавр, і магістр — ступені вищого утворення. Різниця між ними, полягає в тім, що бакалавр орієнтований на практичну діяльність, а магістр — на наукову і викладацьку роботу.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"> Важливою віхою Болонского процесу є впровадження обліку обсягу знань у спеціальних одиницях, називаних кредитами. Зміст <b>європейської кредитно-трансферної системи (ECTS)</b> легко пояснити на конкретному прикладі. Якщо студент прослухав і успішно здав курс математичного аналізу в одному університеті, то у випадку переходу в іншій йому не треба слухати і здавати курс заново: на новому місці навчання йому зарахують зароблені кредити. І навіть якщо в новому вузі відповідний курс в обраній спеціальності буде називатися вже не «математичний аналіз», а якось інакше, його не потрібно буде перездавати. Таким чином, система кредитів — інструмент уніфікації вимог до навчання в різних університетах, що дозволяє студентові без праці переміщатися з одного вузу в іншій, з однієї держави в інше.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"> Оскільки навчальні кредити зберігаються довічно, то, скажемо, інженерові, що вирішив перекваліфікуватися в економісти, не треба буде гаяти час на вторинне «проходження» того ж математичного аналізу. Крім того, якщо той же інженер захоче вивчити нові для себе аспекти професії, записавши на спеціальні курси при вузі, те його загальний освітній залік поповниться кредитами післядипломного утворення, і це дасть йому додаткові переваги на ринку праці. Система кредитів забезпечує не тільки мобільність, але і накопичувальну систему утворення протягом життя.</p>
                                                <div style="float: right;" class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                                        <div class="thumbnail">
                                                            <img  class="img-responsive" src="/uploads/cms-images/bologna_system_3.jpg" alt="Pass exams on A">
                                                        </div>
                                                </div>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">Істотний аспект Болонського процесу — лібералізація «освітнього меню», його підбір у відповідності зі здібностями, бажаннями й інтересами кожного студента. Набір предметів, досліджуваних студентом по обраній спеціальності, поділяється на три групи. Перша група містить обов\'язкові дисципліни, вивчення яких відбувається в строго визначеній послідовності — так, як це робиться зараз. В другу групу входять теж обов\'язкові предмети, однак студент вільний сам вирішувати, у якому семестрі їхній вивчати. Третя ж група є цілком вільною для вибору, навчальним планом задаються тільки загальні напрямки вибору. Скажемо, студентам-«технарям» ставиться в обов\'язок одержати визначена кількість кредитів по дисциплінах, що входить у серію «гуманітарних» курсів, причому, саме за ними залишається рішення, що саме з пропонованого набору кожний з них буде вивчати.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">Допомогти студентам у виборі такого роду індивідуальної «освітньої траєкторії» покликаний «тьютор», або куратор — помічник, захисник, адвокат студента. Для реалізації такого підходу необхідна модульна система, що дозволяє зберегти нормальну послідовність вивчення тих або інших наукових дисциплін. Якщо студент, наприклад, хоче вивчити квантову механіку, йому спочатку необхідно одержати кредити по математичному аналізі і класичній механіці, що входять у так називаний модуль, що завершується квантовою механікою.</p>
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                                        <div class="thumbnail">
                                                            <img  class="img-responsive" src="/uploads/cms-images/bologna_system_2.jpg" alt="Pass exams on A">
                                                        </div>
                                                </div>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"> У розширений текст Болонської угоди входить положення про оцінюванні знань студентів, засноване, з одного боку, на обліку природних здібностей і старанності кожної конкретної особистості, а з іншої, на порівнянні цих показників з навчальними досягненнями колег, здійснюваного за допомогою статистики. Для цього вводиться п\'ять градацій позитивної оцінки (по перших буквах латинського алфавіту): <b>А (відмінно)</b>, <b>В (дуже добре)</b>, <b>С (добре)</b>, <b>D (задовільно)</b>, <b>Е (посередньо)</b>. Оскільки відсоток освоєння — величина статистична, вводиться й оцінка <b>F(незадовільно)</b>, що означає, що даний курс студентові не зарахованийі і кредити, виділені на нього, не враховуються.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">Невід\'ємної складової Болонського процесу є автономізація вузів з чітким поділом функцій між ними і міністерством. Щоб усі вищеописані кроки привели до більш або менш погодженому результатові, передбачається розробка порівнянних критеріїв і методик, що допомагають досягненню головної мети: створенню єдиної зони європейського вищого утворення з гарантованою якістю.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"><b>Закономірне питання: навіщо воно Європі?</b> Відповідь здається очевидним: Європа поєднується політично й економічно, а Болонська угода сприяє об\'єднанню в освітній сфері. Болонський процес потрібний Європі для забезпечення припливу іноземних фахівців, що задовольняють її освітньому і культурному стандартам.</p>
                                                <div style="float: right;" class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                                        <div class="thumbnail">
                                                            <img  class="img-responsive" src="/uploads/cms-images/bologna_system_4.jpg" alt="Pass exams on A">
                                                        </div>
                                                </div>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">Не менш важливим є спроба Європи за допомогою Болонського процесу підвищити освітню конкурентноздатність у порівнянні зі Сполученими Штатами. На сьогоднішній день в Америку з-за кордону щорічно приїжджають учитися більш 650 тисяч студентів, тоді як на всю Європу приходиться всього 250 тисяч студентів-іноземців. Якщо ж розглянути окремо взаимообмін студентами між Європою і США, то з початку 1990-х років число європейських студентів, що навчаються в США, перевищує число американських студентів, що навчаються і Європі. Але ж «імпортні студенти» вносять істотний вклад в економіку країни навчання: мало того, що платять за своє навчання, вони до того ж витрачають гроші на житло, їжу, одяг, електронну техніку, розваги і т.д.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">Болонський процес стартував у червні 1999 року. Представники 29 європейських країн зібралися в італійському місті Болонья на святкування 900-річчя найстаршого університету в Європі і підписали декларацію про побудову так називаної «Зони європейського вищого утворення». Головною ідеєю декларації є координації політики в області утворення і курс на зближення національних освітніх програм. На сьогоднішній день до Болонської угоди приєдналися 43 європейські країни, включаючи Росію та Україну.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"><b>Декларація включає такі основні положення:</b></p>
                                                <ul>
                                                    <b>
                                                        <li>прийняття системи ступенів, що співставляються;</li>
                                                        <li>прийняття системи з двома основними циклами навчання: незавершена вища / завершена вища освіта;</li>
                                                        <li>впровадження системи освітніх кредитів (ECTS);</li>
                                                        <li>підвищення мобільності студентів та викладачів;</li>
                                                        <li>підвищення європейської співпраці в галузі якості освіти;</li>
                                                        <li>підвищення престижу вищої європейської освіти в світі.</li>
                                                    </b>
                                                </ul>
                                                <br>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">Основою, на якій можливе проходження інтеграційних процесів в європейських системах вищої освіти, є <b>кредитно-модульна система (КМС)</b> підготовки фахівців.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"><b>Вона базується на таких засадах:</b></p>
                                                <ul>
                                                    <b>
                                                        <li>комплексні (модульні) програми підготовки фахівців;</li>
                                                        <li>система перезарахувань залікових одиниць (кредитів) (ECTS).</li>
                                                    </b>
                                                </ul>
                                                <br>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"><b>ECTS (European Credit Transfer System)</b> - Європейська система перезарахування кредитів (залікових одиниць трудомісткості) є однією з передумов створення відкритої європейської зони освіти і підготовки, де студенти можуть переміщуватися без перешкод.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"><b>Вона базується на трьох ключових елементах:</b></p>
                                                <ul>
                                                    <b>
                                                        <li>інформації (стосовно навчальних програм і здобутків студентів);</li>
                                                        <li>взаємній угоді (між закладами-партнерами і студентом);</li>
                                                        <li>використанні кредитів ECTS для визначення навчального навантаження для студентів.</li>
                                                    </b>
                                                </ul>
                                                <br>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"><b>А що від цього виграє Україна?</b> Як затверджують західні експерти, швидкість розвитку науково-технічного прогресу така, що трьом чвертям сьогоднішніх студентів і випускників вузів досить незабаром прийдеться працювати по спеціальностях, яких сьогодні ще немає в природі. Нинішнього твердого, забюрократизованого, ієрархічна українського вищого утворення, що дісталася нам в. спадщина з радянських часів, просто не здатна справитися з вимогами часу.</p>
                                                <div style="float: right;" class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                                                        <div class="thumbnail">
                                                            <img  class="img-responsive" src="/uploads/cms-images/bologna_system_5.jpg" alt="Pass exams on A">
                                                        </div>
                                                </div>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">З погляду студентів і викладачів, участь у європейському єдиному освітньому просторі дасть їм нові можливості і розширить обрії, зокрема за рахунок передбаченого в рамках Болонської угоди міжвузівскої і міждержавного обміну. До слова, без такого масового обміну студентами і викладачами Україні буде важко вирішити найгострішу проблему дуже слабкого знання нашими фахівцями іноземних мов. Крім того, особиста присутність допоможе українцям налагодити ділові зв\'язки і зрозуміти ситуацію на європейському ринку. Це особливо ясно видно на прикладі економічного і юридичного утворення. Хіба можна виростити висококласного фахівця, якщо його учать професори, що не приймають участі в розробці і фінансуванні великих ефективних проектів, що не мають реального досвіду в області міжнародного права?.. Така ж проблема коштує і перед нашими інженерами, конструкторами, програмістами... Адже щоб випускати по-справжньому конкурентноздатну продукцію, потрібно насамперед не витрачати сили на «винахід велосипеда».</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph">З огляду на стан української системи вищого утворення, головним напрямком Болонського процесу для нас стане лібералізація вищої школи, навчання студентів принципам волі й одночасно відповідальності за свій вибір. Адже тільки внутрішньо вільна людина, що освоїла мистецтво прийняття рішень і тримання за них відповіді, може бути по-справжньому свідомим громадянином, а також конкурентноздатним, мобільним професіоналом. Західна практика показує, що студент, що вибрав курс на основі особистого інтересу, і учиться зовсім по-іншому.</p>
                                                <p align="left" class="fullwidth-align jumbotron-paragraph"> Треба підкреслити, що головні положення Болонської угоди, описані вище, не є скоростиглий винахід укладачів документа. Усі вони пройшли апробацію часом і довели свою ефективність — у тих же США, і Великобританії й у багатьох країнах і вузах Західної Європи. Саме тому європейці вирішили зробити них загальноприйнятими. Відмінності між ними від країни до країни несуттєві і носять «технічний» характер. Так, наприклад, американські і європейські кредити мають різну «розмірність»: якщо американський студент повинний за рік одержати всього 30 кредитів, те європейський — 60. Це зв\'язано з тим, що в залікові одиниці європейської системи ESTC включений не тільки годинник прослуханих лекцій і виконаних лабораторних робіт, але і самостійна робота студента. До слова, англійські залікові одиниці відрізняються і від американських, і від європейських, студентові мрячного Альбіону за рік необхідно одержати 120 кредитів. </p>
                                            </div>',
            'is_published'      =>  '1',
            'slug'              =>  'bologna-system',
        ]);
        $this->insert('pages',[
            'title'             =>  'Методичні вказівки',
            'meta_description'  =>  'Стандарт ХНУ: Текстові документи. Загальні вимоги.',
            'meta_keyword'      =>  'стандарт, ХНУ, вимоги, вказівки',
            'short_description' =>  '<p align="left" class="fullwidth-align jumbotron-paragraph">Цей стандарт встановлює єдиний порядок оформлення текстових документів, що виконуються в університеті, та поширюється на всі підрозділи університету.</p>
                                    <br>
                                        <div align="center">
                                                <a class="btn btn-primary btn-lg" href="/uploads/cms-files/text_doc.zip">Завантажити методичні вказівки</a>
                                        </div>',
            'description'       =>  '
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="row">
                                                <div class="alert-own alert-info" role="alert">
                                                    <h3 align="center">Стандарт ХНУ: Текстові документи. Загальні вимоги</h3>
                                                    <p align="left" class="fullwidth-align jumbotron-paragraph">Цей стандарт встановлює єдиний порядок оформлення текстових документів, що виконуються в університеті, та поширюється на всі підрозділи університету.</p>
                                                </div>
                                                <div align="center">
                                                                <a class="btn btn-primary btn-lg" href="/uploads/cms-files/text_doc.zip">Завантажити методичні вказівки</a>
                                                        </div>
                                            </div>       
                                        </div>',
            'is_published'      =>  '1',
            'slug'              =>  'methodical-instructions',
        ]);

    }

    public function down()
    {
        $this->delete('pages',['slug'=>'welcome-to-ksm']);
        $this->delete('pages',['slug'=>'what-does-it-mean-computer-engineering']);
        $this->delete('pages',['slug'=>'history-of-kafedra']);
        $this->delete('pages',['slug'=>'curricula']);
        $this->delete('pages',['slug'=>'scientific-activity']);
        $this->delete('pages',['slug'=>'students-leisure']);
        $this->delete('pages',['slug'=>'characteristic-speciality']);
        $this->delete('pages',['slug'=>'bologna-system']);
        $this->delete('pages',['slug'=>'methodical-instructions']);
    }

}
