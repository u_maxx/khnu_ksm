<?php

use yii\db\Schema;
use yii\db\Migration;

class m151017_122919_add_home_page_settings_for_cms extends Migration
{
    public function up()
    {
        $this->insert('settings', [
            'name' => 'cms_page_on_top',
            'value' => '1',
            'description' => 'CMS сторінка у верхній частині на головній'
        ]);

        $this->insert('settings', [
            'name' => 'cms_page_on_bottom',
            'value' => '2',
            'description' => 'CMS сторінка у нижній частині на головній'
        ]);
    }

    public function down()
    {
        $this->delete('settings', ['name' => 'cms_page_on_top']);
        $this->delete('settings', ['name' => 'cms_page_on_bottom']);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
