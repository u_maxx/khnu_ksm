<?php

use yii\db\Schema;
use yii\db\Migration;

class m151011_175159_add_links_table extends Migration
{
    public function up()
    {
        $this->execute("
          CREATE TABLE IF NOT EXISTS `links` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `title` varchar(255) NOT NULL,
          `url` varchar(255) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
    }

    public function down()
    {
        $this->dropTable('links');
    }
}
