<?php

use yii\db\Schema;
use yii\db\Migration;

class m151025_104856_init_main_category extends Migration
{
    public function up()
    {
        $this->insert('categories',[
            'name'          =>  'Головний розділ',
            'parent_id'   =>  '0'
        ]);
    }

    public function down()
    {
        $this->delete('categories', ['name'=>'Головний розділ']);
    }

}
