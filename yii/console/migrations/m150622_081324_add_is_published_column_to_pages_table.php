<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_081324_add_is_published_column_to_pages_table extends Migration
{
    public function up()
    {
        $this->addColumn('pages', 'is_published', 'boolean DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('pages', 'is_published');
    }
}
