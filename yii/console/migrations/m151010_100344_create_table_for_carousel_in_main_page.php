<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_100344_create_table_for_carousel_in_main_page extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `slider`
        (
            `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            `title` VARCHAR(255) NOT NULL,
            `description` VARCHAR(255) NOT NULL,
            `image` VARCHAR(255),
            `link` VARCHAR(255)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
        $this->execute("ALTER TABLE `slider` ADD CONSTRAINT `unique_image` UNIQUE (image);");
    }

    public function down()
    {
        $this->dropTable("slider");
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
