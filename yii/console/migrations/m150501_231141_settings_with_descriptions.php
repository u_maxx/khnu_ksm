<?php

use yii\db\Schema;
use yii\db\Migration;

class m150501_231141_settings_with_descriptions extends Migration
{
    public function up()
    {
        $this->addColumn('settings','description',
            Schema::TYPE_STRING . " NOT NULL"
        );
    }

    public function down()
    {
        $this->dropColumn('settings','description');
    }

}
