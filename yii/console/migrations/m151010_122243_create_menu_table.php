<?php

use yii\db\Schema;
use yii\db\Migration;

class m151010_122243_create_menu_table extends Migration
{
    public function up()
    {
        $this->createTable('menu', [
            'id' => Schema::TYPE_PK,
            'item_type' => Schema::TYPE_SMALLINT,
            'item_id' => Schema::TYPE_BIGINT,
            'parent_id' => Schema::TYPE_BIGINT,
        ]);
    }

    public function down()
    {
        $this->dropTable('menu');
    }

}
