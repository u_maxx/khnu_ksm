<?php

use yii\db\Schema;
use yii\db\Migration;

class m151023_135302_initialize_scientific_activities extends Migration
{
    public function up()
    {
        $this->insert('scientific_activities',[
            'name' => 'Комп\'ютерні мережі'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Паралельні обчислення з використанням технологій MPI, OpenMP, GPU NVIDIA CUDA'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Високопродуктивні обчислення для вирішення класу задач лінійної алгебри'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Теорія і практика побудови комп\'ютерних мереж на базі стека протоколів ТСР/ІР'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Web-технології'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Системи автоматизованого проектування'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Інтелектуальне діагностування дистанційно віддалених цифрових пристроїв'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Дистанційне діагностування комп\'ютерних систем'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Адаптивне діагностування комп\'ютерних мереж'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Захист інформації'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Розробка інформаційних систем'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Системи штучного інтелекту'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Захист інформації'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Інформаційне забезпечення'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Теорія ігр'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Випадкові процеси'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Біотехнічні відмовостійкі системи'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Паралельні комп\'ютерні системи'
        ]);
        $this->insert('scientific_activities',[
            'name' => 'Тестове діагностування цифрових пристроїв'
        ]);

    }

    public function down()
    {
        $this->delete('scientific_activities',[
            'name' => 'Комп\'ютерні мережі'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Паралельні обчислення з використанням технологій MPI, OpenMP, GPU NVIDIA CUDA'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Високопродуктивні обчислення для вирішення класу задач лінійної алгебри'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Теорія і практика побудови комп\'ютерних мереж на базі стека протоколів ТСР/ІР'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Web-технології'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Системи автоматизованого проектування'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Інтелектуальне діагностування дистанційно віддалених цифрових пристроїв'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Дистанційне діагностування комп\'ютерних систем'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Адаптивне діагностування комп\'ютерних мереж'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Захист інформації'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Розробка інформаційних систем'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Системи штучного інтелекту'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Захист інформації'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Інформаційне забезпечення'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Теорія ігр'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Випадкові процеси'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Біотехнічні відмовостійкі системи'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Паралельні комп\'ютерні системи'
        ]);
        $this->delete('scientific_activities',[
            'name' => 'Тестове діагностування цифрових пристроїв'
        ]);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
