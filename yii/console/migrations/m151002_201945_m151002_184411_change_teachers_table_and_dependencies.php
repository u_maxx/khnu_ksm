<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_201945_m151002_184411_change_teachers_table_and_dependencies extends Migration
{
    public function up()
    {
        $this->addColumn('teachers', 'name', 'VARCHAR(255) NOT NULL');
        $this->addColumn('teachers', 'email', 'VARCHAR(255) NULL');
        $this->addColumn('teachers', 'image', 'VARCHAR(255) UNIQUE NULL');
        $this->addColumn('teachers', 'about', 'LONGTEXT NOT NULL');
        $this->execute("ALTER TABLE `publications` MODIFY COLUMN about LONGTEXT NULL;");
        $this->execute("ALTER TABLE `teachers` DROP FOREIGN KEY `FK_user_teacher`;
		                DROP INDEX `FK_user_teacher` ON `teachers`;");
    }

    public function down()
    {
        $this->dropColumn('teachers', 'name');
        $this->dropColumn('teachers', 'email');
        $this->dropColumn('teachers', 'image');
        $this->dropColumn('teachers', 'about');
        $this->execute("ALTER TABLE `publications` MODIFY COLUMN about LONGTEXT NOT NULL;");
        $this->execute("ALTER TABLE `teachers` ADD INDEX `FK_user_teacher` (`id`);
                        ALTER TABLE `teachers` ADD CONSTRAINT `FK_user_teacher` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE;");
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
