<?php

use yii\db\Schema;
use yii\db\Migration;

class m150711_151914_add_slug_to_pages extends Migration
{
    public function up()
    {
        $this->addColumn('pages', 'slug', 'string UNIQUE NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('pages', 'slug');
    }
}
