<?php

use yii\db\Schema;
use yii\db\Migration;

class m150502_075605_insert_basic_settings extends Migration
{
    public function up()
    {
        $this->insert('settings',[
            'name'          =>  'site_title',
            'value'         =>  'КСМ',
            'description'   =>  'Назва сайту'
        ]);

        $this->insert('settings',[
            'name'          =>  'blog_post_per_page',
            'value'         =>  '5',
            'description'   =>  'Кількість записів з блогу на одній сторінці'
        ]);
    }

    public function down()
    {
        $this->delete('settings',['name'=>'site_title']);
        $this->delete('settings',['name'=>'blog_post_per_page']);
    }

}
