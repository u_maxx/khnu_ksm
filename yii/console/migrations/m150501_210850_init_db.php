<?php

use yii\db\Schema;
use yii\db\Migration;

class m150501_210850_init_db extends Migration
{
    public function up()
    {
        $this->execute("
          CREATE TABLE IF NOT EXISTS `articles` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `category_id` int(11) NOT NULL,
          `author_id` int(11) NOT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          `title` varchar(255) NOT NULL,
          `meta_description` varchar(255) NOT NULL,
          `meta_keyword` varchar(255) NOT NULL,
          `short_description` text NOT NULL,
          `description` longtext NOT NULL,
          `is_published` tinyint(1) NOT NULL,
          PRIMARY KEY (`id`),
          KEY `FK_cat` (`category_id`),
          KEY `FK_author_id` (`author_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute("CREATE TABLE IF NOT EXISTS `categories` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(255) NOT NULL,
          `parent_id` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          KEY `FK_parent_id` (`parent_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute("
              CREATE TABLE IF NOT EXISTS `files` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `owner` int(11) NOT NULL,
              `path` varchar(255) NOT NULL,
              `name` varchar(255) NOT NULL,
              `original_name` varchar(255) NOT NULL,
              `uploaded_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `FK_user` (`owner`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute("CREATE TABLE IF NOT EXISTS `pages` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `title` varchar(255) NOT NULL,
          `meta_description` varchar(255) NOT NULL,
          `meta_keyword` varchar(255) NOT NULL,
          `short_description` text NOT NULL,
          `description` longtext NOT NULL,
          `show_in_menu` tinyint(1) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute(
            "CREATE TABLE IF NOT EXISTS `publications` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(255) NOT NULL,
              `year` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `publishing_house` varchar(255) NOT NULL,
              `cout_page` int(11) NOT NULL,
              `about` text NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
        );

        $this->execute("
                CREATE TABLE IF NOT EXISTS `publications_to_teachers` (
              `teacher_id` int(11) NOT NULL,
              `publication_id` int(11) NOT NULL,
              KEY `FK_publication_id` (`publication_id`),
              KEY `FK_teacher_id` (`teacher_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
           CREATE TABLE IF NOT EXISTS `scientific_activities` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(255) NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `sc_activities_to_teachers` (
              `teacher_id` int(11) NOT NULL,
              `sc_activity_id` int(11) NOT NULL,
              KEY `FK_teacher_sc_act_id` (`teacher_id`),
              KEY `FK_scact_id` (`sc_activity_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `settings` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(255) NOT NULL,
              `value` varchar(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute("
          CREATE TABLE IF NOT EXISTS `subjects` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(255) NOT NULL,
          `about` text NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute("
          CREATE TABLE IF NOT EXISTS `subjects_to_teachers` (
          `teacher_id` int(11) NOT NULL,
          `subject_id` int(11) NOT NULL,
          KEY `FK_subjteach_id` (`teacher_id`),
          KEY `FK_subj_id` (`subject_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
              CREATE TABLE IF NOT EXISTS `teachers` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `degree` varchar(255) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `FK_user_teacher` (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute("
               CREATE TABLE IF NOT EXISTS `users` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `login` varchar(255) NOT NULL,
              `email` varchar(255) NOT NULL,
              `password_hash` varchar(255) NOT NULL,
              `password_reset_token` varchar(255),
              `auth_key` varchar(32) NOT NULL,
              `group` int(1) NOT NULL,
              `status` tinyint(1) NOT NULL,
              `is_teacher` tinyint(1) NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `first_name` varchar(255),
              `second_name` varchar(255),
              `third_name` varchar(255),
              `about` text,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

        $this->execute("
            ALTER TABLE `articles`
              ADD CONSTRAINT `FK_author_id` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
              ADD CONSTRAINT `FK_cat` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
        ");

        $this->execute("
            ALTER TABLE `categories`
              ADD CONSTRAINT `FK_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
        ");

        $this->execute("
            ALTER TABLE `files`
              ADD CONSTRAINT `FK_user` FOREIGN KEY (`owner`) REFERENCES `users` (`id`) ON DELETE CASCADE;
        ");

        $this->execute("
            ALTER TABLE `publications_to_teachers`
              ADD CONSTRAINT `FK_publication_id` FOREIGN KEY (`publication_id`) REFERENCES `publications` (`id`) ON DELETE CASCADE,
              ADD CONSTRAINT `FK_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE;
        ");

        $this->execute("
            ALTER TABLE `sc_activities_to_teachers`
              ADD CONSTRAINT `FK_teacher_sc_act_id` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE,
              ADD CONSTRAINT `FK_scact_id` FOREIGN KEY (`sc_activity_id`) REFERENCES `scientific_activities` (`id`) ON DELETE CASCADE;
        ");

        $this->execute("
            ALTER TABLE `subjects_to_teachers`
              ADD CONSTRAINT `FK_subj_id` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
              ADD CONSTRAINT `FK_subjteach_id` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE;
        ");

        $this->execute("
            ALTER TABLE `teachers`
              ADD CONSTRAINT `FK_user_teacher` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
        ");
    }

    public function down()
    {
        $this->dropForeignKey("FK_user", 'files');
        $this->dropForeignKey("FK_parent_id", 'categories');
        $this->dropForeignKey("FK_publication_id", 'publications_to_teachers');
        $this->dropForeignKey("FK_author_id","articles");
        $this->dropForeignKey("FK_cat","articles");
        $this->dropForeignKey("FK_teacher_id","publications_to_teachers");
        $this->dropForeignKey("FK_teacher_sc_act_id","sc_activities_to_teachers");
        $this->dropForeignKey("FK_scact_id","sc_activities_to_teachers");
        $this->dropForeignKey("FK_subj_id","subjects_to_teachers");
        $this->dropForeignKey("FK_subjteach_id","subjects_to_teachers");
        $this->dropForeignKey("FK_user_teacher","teachers");

        $this->dropTable("files");
        $this->dropTable("pages");
        $this->dropTable("publications");
        $this->dropTable("publications_to_teachers");
        $this->dropTable("scientific_activities");
        $this->dropTable("sc_activities_to_teachers");
        $this->dropTable("settings");
        $this->dropTable("subjects");
        $this->dropTable("subjects_to_teachers");
        $this->dropTable("teachers");
        $this->dropTable("articles");
        $this->dropTable("users");
        $this->dropTable("categories");
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
