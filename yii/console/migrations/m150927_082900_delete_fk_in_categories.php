<?php

use yii\db\Schema;
use yii\db\Migration;

class m150927_082900_delete_fk_in_categories extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `categories` DROP FOREIGN KEY `FK_parent_id`;
                        DROP INDEX `FK_parent_id` ON `categories`;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `categories` ADD INDEX `FK_parent_id` (`parent_id`);
                        ALTER TABLE `categories` ADD CONSTRAINT `FK_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;");

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
