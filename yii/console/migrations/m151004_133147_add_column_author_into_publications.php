<?php

use yii\db\Schema;
use yii\db\Migration;

class m151004_133147_add_column_author_into_publications extends Migration
{
    public function up()
    {
        $this->addColumn('publications', 'author', 'VARCHAR(255)');
    }

    public function down()
    {
        $this->dropColumn('publications', 'author');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
