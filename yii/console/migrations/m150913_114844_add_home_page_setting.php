<?php

use yii\db\Schema;
use yii\db\Migration;

class m150913_114844_add_home_page_setting extends Migration
{
    public function up()
    {
        $this->insert('settings', [
            'name' => 'home_page_id',
            'value' => '0',
            'description' => 'Домашня сторінка'
        ]);
    }

    public function down()
    {
        $this->delete('settings', ['name' => 'home_page_id']);
    }

}