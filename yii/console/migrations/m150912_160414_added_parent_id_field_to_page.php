<?php

use yii\db\Schema;
use yii\db\Migration;

class m150912_160414_added_parent_id_field_to_page extends Migration
{
    public function up()
    {
        $this->addColumn('pages', 'parent_id', 'int(11)');
    }

    public function down()
    {
        $this->dropColumn('pages', 'parent_id');
    }
}
