<?php

use yii\db\Schema;
use yii\db\Migration;

class m150925_112804_add_slug_into_articles extends Migration
{
    public function up()
    {
        $this->addColumn('articles', 'slug', 'string UNIQUE NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('articles', 'slug');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
