<?php

use yii\db\Schema;
use yii\db\Migration;

class m151019_141133_add_setting_for_site_name extends Migration
{
    public function up()
    {
        $this->insert('settings',[
            'name'          =>  'site_name',
            'value'         =>  'Кафедра комп\'ютерних систем та мереж ХНУ',
            'description'   =>  'Повна назва сайту'
        ]);

    }

    public function down()
    {
        $this->delete('settings', ['name' => 'site_name']);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
