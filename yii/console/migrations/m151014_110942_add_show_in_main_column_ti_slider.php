<?php

use yii\db\Schema;
use yii\db\Migration;

class m151014_110942_add_show_in_main_column_ti_slider extends Migration
{
    public function up()
    {
        $this->addColumn('slider', 'show_in_main', 'TINYINT');
    }

    public function down()
    {
        $this->dropColumn('slider', 'show_in_main');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
