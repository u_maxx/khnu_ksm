<?php

use yii\db\Schema;
use yii\db\Migration;

class m150928_122303_add_article_image_column extends Migration
{
    public function up()
    {
        $this->addColumn('articles', 'image', 'VARCHAR(255) UNIQUE NULL');
    }

    public function down()
    {
        $this->dropColumn('articles', 'image');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
