<?php

use yii\db\Schema;
use yii\db\Migration;

class m151011_163122_remove_unuseful_field_from_pages extends Migration
{
    public function up()
    {
        $this->dropColumn('pages', 'parent_id');
        $this->dropColumn('pages', 'show_in_menu');

    }

    public function down()
    {
        $this->addColumn('pages', 'parent_id', 'int(11)');
        $this->addColumn('pages', 'show_in_menu', 'int(1)');
    }
}
