<?php

use yii\db\Schema;
use yii\db\Migration;

class m150704_210524_insert_admin_sample_data extends Migration
{
    public function up()
    {
        $this->insert('users', [
            'login' => 'test',
            'email'  =>  'test@test.com',
            'password_hash' => '$2y$13$w2Go28udG0tt8EFalWueGuFB3cIppWrb6zKqSqJSlNPepby6cS.sq',
            'password_reset_token' => 'r-3EnLlK3jdmoDa3LxDdXElQ4lfcwcAG_1436030908',
            'auth_key' => 'QEerKKaJ0DY74Nwb_0v1qVLK9HmeQbr7',
            'status' => 1,
            'group' => 2,
            'is_teacher' => 0,
        ]);
    }

    public function down()
    {
        $this->delete('users',[
            'login' => 'test',
        ]);
    }

}
