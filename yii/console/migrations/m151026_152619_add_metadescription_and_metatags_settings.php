<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_152619_add_metadescription_and_metatags_settings extends Migration
{
    public function up()
    {
        $this->insert('settings', [
            'name'          =>  'blog_meta_key',
            'value'         =>  'КСМ, ХНУ, новини, студенти, кафедра, навчання, університет',
            'description'   =>  'Мета теги блогу'
        ]);
        $this->insert('settings', [
            'name'          =>  'blog_meta_description',
            'value'         =>  'Усі новини Кафедри Комп\'ютерних систем та мереж Хмельницького національного університету.',
            'description'   =>  'Мета опис блогу'
        ]);
        $this->insert('settings', [
            'name'          =>  'lecturers_meta_key',
            'value'         =>  'Викладачі, КСМ, публікації, предмети, кафедра, ХНУ',
            'description'   =>  'Мета теги сторінки про викладачів'
        ]);
        $this->insert('settings', [
            'name'          =>  'lecturers_meta_description',
            'value'         =>  'Інформація про викладачів кафедри Комп\'ютерних систем та мереж Хмельницького національного університету та їх біографії.',
            'description'   =>  'Мета опис сторінки про викладачів'
        ]);
        $this->insert('settings', [
            'name'          =>  'subjects_meta_key',
            'value'         =>  'Предмети, дисципліни, КСМ, кафедра, ХНУ',
            'description'   =>  'Мета теги сторінки про предмети'
        ]);
        $this->insert('settings', [
            'name'          =>  'subjects_meta_description',
            'value'         =>  'Інформація про предмети, які вивчаються на кафедрі Комп\'ютерних систем та мереж Хмельницького національного університету.',
            'description'   =>  'Мета опис сторінки про предмети'
        ]);
        $this->insert('settings', [
            'name'          =>  'contacts_meta_key',
            'value'         =>  'Контакти, телефон, КСМ, зв\'язок, адреса, знайти, кафедра, ХНУ',
            'description'   =>  'Мета теги сторінки контактів кафедри'
        ]);
        $this->insert('settings', [
            'name'          =>  'contacts_meta_description',
            'value'         =>  'Контактна нформація та адреса кафедри Комп\'ютерних систем та мереж Хмельницького національного університету, відправити повідомлення.',
            'description'   =>  'Мета опис сторінки контактів кафедри'
        ]);
        $this->insert('settings', [
            'name'          =>  'main_meta_key',
            'value'         =>  'Контакти, КСМ, мереж, вступ, навчання, студент, університет, кафедра, ХНУ',
            'description'   =>  'Мета теги головної сторінки сайту'
        ]);
        $this->insert('settings', [
            'name'          =>  'main_meta_description',
            'value'         =>  'Офіційний сайт кафедри Комп\'ютерних систем та мереж Хмельницького національного університету, новини та інформація кафедри.',
            'description'   =>  'Мета опис головної сторінки сайту'
        ]);
    }

    public function down()
    {
        $this->delete('settings', ['name' => 'blog_meta_key']);
        $this->delete('settings', ['name' => 'blog_meta_description']);
        $this->delete('settings', ['name' => 'lecturers_meta_key']);
        $this->delete('settings', ['name' => 'lecturers_meta_description']);
        $this->delete('settings', ['name' => 'subjects_meta_key']);
        $this->delete('settings', ['name' => 'subjects_meta_description']);
        $this->delete('settings', ['name' => 'contacts_meta_key']);
        $this->delete('settings', ['name' => 'contacts_meta_description']);
        $this->delete('settings', ['name' => 'main_meta_key']);
        $this->delete('settings', ['name' => 'main_meta_description']);
    }

}
