<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_194712_initialize_categories_of_blog extends Migration
{
    public function up()
    {
        $this->insert('categories', [
            'name'          =>  'Кафедра',
            'parent_id'     =>  '1',
        ]);
        $this->insert('categories', [
            'name'          =>  'Університет',
            'parent_id'     =>  '1',
        ]);
        $this->insert('categories', [
            'name'          =>  'Навчальний процес',
            'parent_id'     =>  '1',
        ]);
        $this->insert('categories', [
            'name'          =>  'Наукова діяльність',
            'parent_id'     =>  '1',
        ]);
        $this->insert('categories', [
            'name'          =>  'Дозвілля',
            'parent_id'     =>  '1',
        ]);
        $this->insert('categories', [
            'name'          =>  'Різне',
            'parent_id'     =>  '1',
        ]);
        $this->insert('categories', [
            'name'          =>  'Новини кафедри',
            'parent_id'     =>  '2',
        ]);
        $this->insert('categories', [
            'name'          =>  'Досягнення кафедри',
            'parent_id'     =>  '2',
        ]);
        $this->insert('categories', [
            'name'          =>  'Зміни в навчальному процесі',
            'parent_id'     =>  '4',
        ]);
        $this->insert('categories', [
            'name'          =>  'Студентам',
            'parent_id'     =>  '4',
        ]);
        $this->insert('categories', [
            'name'          =>  'Новини університету',
            'parent_id'     =>  '3',
        ]);
        $this->insert('categories', [
            'name'          =>  'Абітурієнтам',
            'parent_id'     =>  '3',
        ]);
        $this->insert('categories', [
            'name'          =>  'Студенстська активність',
            'parent_id'     =>  '5',
        ]);
        $this->insert('categories', [
            'name'          =>  'Розробки кафедри',
            'parent_id'     =>  '5',
        ]);
        $this->insert('categories', [
            'name'          =>  'Наукові дослідження',
            'parent_id'     =>  '5',
        ]);
        $this->insert('categories', [
            'name'          =>  'Студентське дозвілля',
            'parent_id'     =>  '6',
        ]);
        $this->insert('categories', [
            'name'          =>  'Соцмережі',
            'parent_id'     =>  '6',
        ]);
    }

    public function down()
    {
        $this->delete('categories', ['name' => 'Кафедра']);
        $this->delete('categories', ['name' => 'Університет']);
        $this->delete('categories', ['name' => 'Навчальний процес']);
        $this->delete('categories', ['name' => 'Наукова діяльність']);
        $this->delete('categories', ['name' => 'Дозвілля']);
        $this->delete('categories', ['name' => 'Різне']);
        $this->delete('categories', ['name' => 'Новини кафедри']);
        $this->delete('categories', ['name' => 'Досягнення кафедри']);
        $this->delete('categories', ['name' => 'Зміни в навчальному процесі']);
        $this->delete('categories', ['name' => 'Студентам']);
        $this->delete('categories', ['name' => 'Новини університету']);
        $this->delete('categories', ['name' => 'Абітурієнтам']);
        $this->delete('categories', ['name' => 'Студенстська активність']);
        $this->delete('categories', ['name' => 'Розробки кафедри']);
        $this->delete('categories', ['name' => 'Наукові дослідження']);
        $this->delete('categories', ['name' => 'Студентське дозвілля']);
        $this->delete('categories', ['name' => 'Соцмережі']);
    }
}
