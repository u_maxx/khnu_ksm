<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_200044_initialize_links extends Migration
{
    public function up()
    {
        $this->insert('links', [
            'title'   =>  'Головна',
            'url'     =>  '/',
        ]);
        $this->insert('links', [
            'title'   =>  'Блог',
            'url'     =>  '/blog/',
        ]);
        $this->insert('links', [
            'title'   =>  'Викладачі',
            'url'     =>  '/lecturers/',
        ]);
        $this->insert('links', [
            'title'   =>  'Предмети',
            'url'     =>  '/subjects/',
        ]);
        $this->insert('links', [
            'title'   =>  'Контакти',
            'url'     =>  '/site/contact',
        ]);
    }

    public function down()
    {
        $this->delete('links', ['title' => 'Головна']);
        $this->delete('links', ['title' => 'Блог']);
        $this->delete('links', ['title' => 'Викладачі']);
        $this->delete('links', ['title' => 'Предмети']);
        $this->delete('links', ['title' => 'Контакти']);
    }
}
