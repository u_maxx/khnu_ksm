<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_190019_fix_active_records_finding_methods_by_adding_id_columns_to_tables extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `publications_to_teachers` ADD `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT;
                        ALTER TABLE `sc_activities_to_teachers` ADD `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT;
                        ALTER TABLE `subjects_to_teachers` ADD `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `publications_to_teachers` DROP `id`;
                        ALTER TABLE `sc_activities_to_teachers` DROP `id`;
                        ALTER TABLE `subjects_to_teachers` DROP `id`;");
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
