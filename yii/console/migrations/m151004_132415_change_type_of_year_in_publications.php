<?php

use yii\db\Schema;
use yii\db\Migration;

class m151004_132415_change_type_of_year_in_publications extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `publications` CHANGE `year` `year` INT;
                        ALTER TABLE `publications` MODIFY `year` INT;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `publications` CHANGE `year` `year` TIMESTAMP;
                        ALTER TABLE `publications` MODIFY `year` TIMESTAMP DEFAULT current_timestamp;");
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
