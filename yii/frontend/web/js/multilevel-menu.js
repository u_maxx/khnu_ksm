$(document).ready(function () {

    $('.dropdown-toggle').on('click', function () {
        $('.navbar-nav').find('.display-submenu').removeClass('display-submenu');
    });

    $('.dropdown-submenu > a').on('click', function (event) {

        var subMenu = $(this).parent().find('.dropdown-menu');

        if(subMenu.parent().find('.display-submenu').length > 0) {
            $('.navbar-nav').find('.display-submenu').removeClass('display-submenu');
            subMenu.addClass('display-submenu');
        } else {
            $('.navbar-nav').find('.display-submenu').removeClass('display-submenu');
        }

        if(subMenu.length > 0){
            // Avoid following the href location when clicking
            event.preventDefault();
            // Avoid having the menu to close when clicking
            event.stopPropagation();
        }

        subMenu.toggleClass('display-submenu');
    });

});