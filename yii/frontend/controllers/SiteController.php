<?php
namespace frontend\controllers;

use common\models\Settings;
use Yii;
use common\models\LoginForm;
use common\models\Rules;
use common\models\Slider;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Page;
use common\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $mainMetaTags = Settings::getSetting('main_meta_key');
        $mainMetaDescription = Settings::getSetting('main_meta_description');
        $pageId = Settings::getSetting('home_page_id');
        $siteFullName = Settings::getSetting('site_name');
        $topPage = Page::findOne(Settings::getSetting('cms_page_on_top'));
        $bottomPage = Page::findOne(Settings::getSetting('cms_page_on_bottom'));
        $slides = Slider::getArrayOfActiveSlides();

        if ($pageId && $pageId > 0) {
            $page = Page::findOne($pageId);
            if ($page !== null) {
                return $this->render('/page/home', [
                    'page' => $page,
                    'siteFullName' => $siteFullName
                ]);
            }
        }

        return $this->render('index', [
            'topPage' => $topPage,
            'bottomPage' => $bottomPage,
            'slides' => $slides,
            'siteFullName' => $siteFullName,
            'mainMetaTags' => $mainMetaTags,
            'mainMetaDescription' => $mainMetaDescription
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('yii', 'You are logged in as ') . Yii::$app->user->identity->login . '.');
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $contactMetaTags = Settings::getSetting('contacts_meta_key');
        $contactMetaDescription = Settings::getSetting('contacts_meta_description');
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', Yii::t('yii','Thank you for contacting us. We will respond to you as soon as possible.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('yii','There was an error sending email.'));
            }
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
                'contactMetaTags' => $contactMetaTags,
                'contactMetaDescription' => $contactMetaDescription
            ]);
        }
    }

    public function actionSignup()
    {
        $registrationRules = Rules::getRule('registration_rules');
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'registrationRules' => $registrationRules
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('yii','Check your email for further instructions.'));

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('yii','Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('yii','New password was saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionUserUpdate()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->getSession()->setFlash('error', Yii::t('yii','You should to log in first.'));
            return $this->redirect(['/site/login']);
        }

        $model = User::findOne(Yii::$app->user->id);

        $model->scenario='user_update';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if (!empty($model->password)) $model->setPassword($model->password);

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'User info has been updated.'));
                return $this->redirect(['index']);
            }

            Yii::$app->getSession()->setFlash('error', $model->errors);
        }

        return $this->render('userUpdate', [
            'model' => $model,
        ]);
    }

}
