<?php
namespace frontend\controllers;

use common\models\Teachers;
use common\models\Publications;
use common\models\Settings;

class LecturersController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $lecturersMetaTags = Settings::getSetting('lecturers_meta_key');
        $lecturersMetaDescription = Settings::getSetting('lecturers_meta_description');
        $lecturers = Teachers::find();
        return $this->render('index', ['lecturers' => $lecturers, 'lecturersMetaTags' => $lecturersMetaTags, 'lecturersMetaDescription' => $lecturersMetaDescription]);
    }

    public function actionPublication($id)
    {
        $publications = Publications::findOne($id);
        return $this->render('publication', ['publication' => $publications]);
    }
}
