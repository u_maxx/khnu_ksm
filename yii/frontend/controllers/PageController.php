<?php

namespace frontend\controllers;

use common\models\Page;
use yii\web\NotFoundHttpException;

class PageController extends \yii\web\Controller
{
    public function actionIndex($slug)
    {
        $model = Page::findOne([
            'slug' => $slug,
            'is_published' => '1',
        ]);

        if ($model !== null) {
            return $this->render('cms_page', [
                'page' => $model
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
