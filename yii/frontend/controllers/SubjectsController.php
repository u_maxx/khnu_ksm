<?php
namespace frontend\controllers;

use common\models\Subjects;
use common\models\Settings;

class SubjectsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $subjectsMetaTags = Settings::getSetting('subjects_meta_key');
        $subjectsMetaDescription = Settings::getSetting('subjects_meta_description');
        $subjects = Subjects::find()->all();
        return $this->render('index', ['subjects' => $subjects, 'subjectsMetaTags' => $subjectsMetaTags, 'subjectsMetaDescription' => $subjectsMetaDescription]);
    }
}
