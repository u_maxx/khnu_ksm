<?php

namespace frontend\controllers;

use yii\web\NotFoundHttpException;
use common\models\Articles;
use common\models\Categories;
use common\models\Settings;
use yii\data\ActiveDataProvider;


class BlogController extends \yii\web\Controller
{
    public function actionArticle($id)
    {

    }

    public function actionIndex($category = null, $id = null)
    {
        $blogMetaTags = Settings::getSetting('blog_meta_key');
        $blogMetaDescription = Settings::getSetting('blog_meta_description');
        if($category !== null) {
            $dataProvider = new ActiveDataProvider([
                'query' => Articles::getArticlesByCategory($category),
                'pagination' => [
                    'pageSize' => Settings::getSetting('blog_post_per_page'),
                ],
            ]);
            $categoryRow = Categories::findOne($category);
            $categoryParentRow = Categories::findOne($categoryRow->parent_id);
            return $this->render('index', ['dataProvider' => $dataProvider, 'categoryName' => $categoryRow->name, 'categoryParentName' => $categoryParentRow->name, 'categoryParentId' => $categoryParentRow->id, 'blogMetaTags' => $blogMetaTags, 'blogMetaDescription' => $blogMetaDescription, 'toDo' => 'blog-home']);
        } elseif($id !== null) {
            $artticle = $this->findModel($id);
            return $this->render('index', ['model' => $artticle, 'toDo' => 'article']);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Articles::find()->where(['is_published' => 1])->orderBy('id desc'),
                'pagination' => [
                    'pageSize' => Settings::getSetting('blog_post_per_page'),
                ],
            ]);
            return $this->render('index', ['dataProvider' => $dataProvider, 'blogMetaTags' => $blogMetaTags, 'blogMetaDescription' => $blogMetaDescription, 'categoryName' => null, 'toDo' => 'blog-home']);
        }
    }

    /**
     * Finds the Articles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
