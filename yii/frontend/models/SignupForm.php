<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $login;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'filter', 'filter' => 'trim'],
            ['login', 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            ['login', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This username has already been taken.')],
            ['login', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This email address has already been taken.')],

            ['password', 'required', 'message' => Yii::t('yii','This field can not be empty.')],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->login = $this->login;
            $user->email = $this->email;

            $user->group=User::GROUP_USER;
            $user->is_teacher=User::TEACHER_FALSE;
            $user->status=User::STATUS_ACTIVE;

            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generatePasswordResetToken();

            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }

    public function attributeLabels(){
        return [
            'login' =>  Yii::t('app', 'Login'),
            'email' =>  Yii::t('app', 'Email'),
            'password' =>  Yii::t('app', 'Password'),
        ];
    }
}
