<?php
/* @var $this yii\web\View */

$this->title = Yii::t('yii', 'Subjects of KSM department');

$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(array(
    'description' => $subjectsMetaDescription
));

$this->registerMetaTag(array(
    'keyword' => $subjectsMetaTags
));
?>

<div class="container wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"><?= $this->title ?></h1>
            <div class="breadcrumbs-outer"></div>
        </div>
    </div>
    <div class="row thumbnail page-content contacts-page clear-margin">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="hr-divider"></div>
            <div class="jumbotron">
                <h1><?= Yii::t('yii','Learn more about KSM department!') ?></h1>
                <p><?= Yii::t('yii','This page contains information about subjects studied by KSM Department of Khmelnitsky National University.') ?></p>
                    <p><?= Yii::t('yii','You can learn more about lecturers of department of KSM and their subjects at the page about lecturers.') ?></p>
                <p><a class="btn btn-primary btn-lg" href="/lecturers/" role="button"><?= Yii::t('yii','Learn more') ?></a></p>
            </div>

            <div class="hr-divider"></div>

            <div class="panel-group" id="accordion">
            <?php foreach($subjects as $subject) : ?>
                <div class="panel panel-default" style="font-size: 18px;">
                    <div class="panel-heading">
                        <h4 class="panel-title" style="padding: 5px 0;">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $subject->id ?>">
                                <?= $subject->name ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse-<?= $subject->id ?>" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?= $subject->about ?>
                            <?php if(count($subject->subjectsToTeachers)): ?>
                            <h3><?= Yii::t('yii','Lecturers'); ?></h3>
                            <ul>
                                <?php foreach($subject->subjectsToTeachers as $teacher) : ?>
                                    <li><?= $teacher->teacher->name  ?><small style="color: #757575;"> (<?= $teacher->teacher->degree  ?>)</small></h4></li>
                                <?php endforeach ?>

                            </ul>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>

            <div class="hr-divider"></div>
        </div>
    </div>
</div>