<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 07.10.15
 * Time: 15:55
 */
use yii\helpers\Html;
?>

<div class="hr-divider"></div>

<h2>
    <a href="<?= Yii::$app->urlManager->createUrl(['blog/index', 'id' => $model->id]) ?>" class="no-underline"><?= $model->title ?></a>
</h2>
<p><i class="glyphicon glyphicon-user"></i> <?= Yii::t('yii', 'Author') ?> <a href="#" class="no-underline"><?= $model->author->first_name . ' ' . $model->author->second_name . ' ' . $model->author->third_name ?></a></p>
<p><i class="fa fa-clock-o"></i> <?= Yii::t('yii', 'Date of publication') ?>: <?= Yii::$app->formatter->asDateTime($model->created_at, 'long') ?> / <i class="glyphicon glyphicon-paperclip"></i> <?= Yii::t('yii', 'Category') ?>: <a href="<?= Yii::$app->urlManager->createUrl(['blog/index', 'category' => $model->category->id]) ?>" class="no-underline"><?= $model->category->name ?></a></p>

<div class="hr-divider" style="margin: 10px 0 15px 0;"></div>

<?php if($model->image): ?>
    <?= Html::img('@web/uploads/article-images/'.$model->image, ['alt' => $model->title, 'style' => 'margin: 0 auto;', 'class' => 'img-responsive img-hover']) ?>
<?php endif ?>


<p><?= $model->short_description  ?></p>

<a class="btn btn-default" href="<?= Yii::$app->urlManager->createUrl(['blog/index', 'id' => $model->id]) ?>"><?= Yii::t('yii', 'Read more') ?> <i class="fa fa-angle-right"></i></a>

<div class="hr-divider"></div>
