<?php
/* @var $this yii\web\View */

$mainTitle = Yii::t('yii', 'Blog of KSM department');
$subTitle = Yii::t('yii', 'All publications');
$this->title = Yii::t('yii', 'Blog of KSM department');


switch($toDo) {
    case 'article':
        $this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Blog'), 'url' => ['index']];
        $this->params['breadcrumbs'][] = $model->title;
        $mainTitle = $model->title;
        $subTitle = '';
        break;
    case 'blog-home':
        if($categoryName) {
            $this->title = Yii::t('yii', 'Category') . ': ' . $categoryName;
            $subTitle = $categoryName;
            $this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Blog'), 'url' => ['index']];
            if($categoryParentId !== 1)
                $this->params['breadcrumbs'][] = ['label' => $categoryParentName, 'url' => ['/blog/index', 'category' => $categoryParentId]];
            $this->params['breadcrumbs'][] = $categoryName;
        } else {
            $this->params['breadcrumbs'][] = Yii::t('yii', 'Blog');
        }
        $this->registerMetaTag(array(
            'description' => $blogMetaDescription
        ));

        $this->registerMetaTag(array(
            'keyword' => $blogMetaTags
        ));
        break;
}
?>

<div class="container wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"><?= $mainTitle ?>
                <small><?= $subTitle ?></small>
            </h1>
            <div class="breadcrumbs-outer"></div>
        </div>
    </div>
    <div class="row thumbnail page-content clear-margin">
        <div id="article-container" class="col-md-8">
            <?php
            switch($toDo) {
                case 'article':
                    echo $this->render('article', ['model' => $model]);
                    break;
                case 'blog-home':
                    echo $this->render('blog-home', ['dataProvider' => $dataProvider]);
                    break;
            }
            ?>
        </div>
        <!-- Сайдбар -->
        <div id="categories-list-container" class="col-md-4">
            <!-- Категорії -->
            <div class="well">
                <h4><?= Yii::t('yii', 'Categories of blog') ?></h4>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $this->render('_categories') ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>