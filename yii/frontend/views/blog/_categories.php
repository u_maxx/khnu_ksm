<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.10.15
 * Time: 0:19
 */
use yii\helpers\Html;
use common\models\Categories;
use common\models\Articles;


$categories = new Categories();
$categoriesTable = $categories->getCategoriesTable();

?>

<?php
echo '<ul class="list-unstyled blog-categories">';

    foreach($categoriesTable as $category)
    {
    if($category['parent_id'] === '1') {
    echo '<li><i class="glyphicon glyphicon-paperclip"></i>'. Html::a(Html::encode($category['name']), [Yii::$app->urlManager->createUrl(['blog/index', 'category' => $category['id']])]) . '<span> (' . Articles::countArticlesInCategory($category['id']) . ')</span></li>';
    echo '<ul class="list-unstyled blog-categories" style="margin-left: 25px;">';

        foreach($categoriesTable as $subCategory)
        {
            if($category['id'] == $subCategory['parent_id']) {
                echo '<li><i class="glyphicon glyphicon-link"></i>' . Html::a(Html::encode($subCategory['name']), [Yii::$app->urlManager->createUrl(['blog/index', 'category' => $subCategory['id']])]) . '<span> (' . Articles::countArticlesInCategory($subCategory['id']) . ')</span></li>';
        }
        }

        echo '</ul>';
    }
    }
    echo '</ul>';
?>