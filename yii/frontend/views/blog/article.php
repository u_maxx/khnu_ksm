<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->registerMetaTag(array(
    'description' => $model->meta_description
));

$this->registerMetaTag(array(
    'keyword' => $model->meta_keyword
));
$this->title = $model->title;
?>

<p>
    <i class="glyphicon glyphicon-user"></i> <?= Yii::t('yii', 'Author') ?>: <a href="" class="no-underline"><?= $model->author->first_name . ' ' . $model->author->second_name . ' ' . $model->author->third_name ?></a>
</p>
<p><i class="fa fa-clock-o"></i> <?= Yii::t('yii', 'Date of publication') ?>: <?= Yii::$app->formatter->asDateTime($model->created_at, 'long') ?> / <i class="glyphicon glyphicon-paperclip"></i> <?= Yii::t('yii', 'Category') ?>: <a href="<?= Yii::$app->urlManager->createUrl(['blog/index', 'category' => $model->category->id]) ?>" class="no-underline"><?= $model->category->name ?></a></p>
<div class="hr-divider"></div>
<?php if($model->image): ?>
    <?= Html::img('@web/uploads/article-images/'.$model->image, ['alt' => $model->title, 'style' => 'margin: 0 auto;', 'class' => 'img-responsive img-hover']) ?>
<?php endif ?>
<div class="hr-divider"></div>
<div class="article-content">
    <?= $model->description ?>
</div>
<div class="hr-divider"></div>

<!-- Blog Comments -->
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'khnuksm';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
<hr>