<?php
/* @var $this yii\web\View */
use evgeniyrru\yii2slick\Slick;
use common\models\Slider;
use common\models\Articles;
use yii\helpers\Html;
use common\models\Page;

$this->title = $siteFullName;

$this->registerMetaTag(array(
    'description' => $mainMetaDescription
));

$this->registerMetaTag(array(
    'keyword' => $mainMetaTags
));
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<?php if(count($slides)): ?>
<header id="carousel-example-generic" class="carousel slide" data-ride="carousel">

      <ol class="carousel-indicators">
          <?php
          $slideIndex = 0;
          foreach ($slides as $slide) {
              if(!$slideIndex)
                  echo '<li data-target="#carousel-example-generic" data-slide-to="' . $slideIndex++ . '" class="active"></li>';
              else
                  echo '<li data-target="#carousel-example-generic" data-slide-to="' . $slideIndex++ . '"></li>';
          }
          ?>
      </ol>
    <div class="carousel-inner">
        <?php
        $slideIndex = 0;
        foreach ($slides as $slide) {
            $slideWrapperBegin = $slideWrapperEnd = 'div';
            $bgStyle = 'background: #aaa;';
            if($slide->link) {
                $slideWrapperBegin = 'a href="' . $slide->link . '" ';
                $slideWrapperEnd = 'a';
            }
            if($slide->image) {
                $bgStyle = 'background-image:url(' . Yii::$app->homeUrl . 'uploads/carousel-images/' . $slide->image . ');';
            }
            if(!$slideIndex)
                echo '<'. $slideWrapperBegin .' class="item active">
                    <div class="fill" style="' . $bgStyle . '">
                        <div class="carousel-caption">
                            <h1>' . $slide->title . '</h1>
                            <p>' . $slide->description . '</p>
                        </div>
                    </div>
                </'. $slideWrapperEnd .'>';
            else
                echo '<'. $slideWrapperBegin .' class="item">
                    <div class="fill" style="' . $bgStyle . '">
                        <div class="carousel-caption">
                            <h1>' . $slide->title . '</h1>
                            <p>' . $slide->description . '</p>
                        </div>
                    </div>
                </'. $slideWrapperEnd .'>';
            $slideIndex++;
        }
        ?>
    </div>


      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="icon-prev"></span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="icon-next"></span>
      </a>
</header>
<?php endif; ?>

<div class="container wrapper">

        <?php if(!empty($topPage)): ?>
        <div class="row jumbotron jumbotron-white" >
            <div class="col-lg-12">
                <h1 class="page-header jumbotron-header">
                    <?= $topPage->title ?>
                </h1>
            </div>
            <div class="col-xs-12">
                <?= $topPage->short_description ?>
                <a class="btn btn-default btn-lg button-mg-bottom" href="/<?= $topPage->slug ?>"><?= Yii::t('yii', 'Read more'); ?> <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <?php endif; ?>

        <div class="hr-divider"></div>

        <div class="row panel panel-default last-news">
            <div class="col-lg-12">
                <h2 class="page-header"><?= Yii::t('yii', 'Last publications in blog of department'); ?></h2>
            </div>
            <div class="clr"></div>

                <?=Slick::widget([

                    'itemContainer' => 'div',

                    'containerOptions' => ['class' => 'test'],

                    'itemOptions' => ['class' => 'col-md-4'],

                    // Items for carousel. Empty array not allowed, exception will be throw, if empty
                    'items' => Articles::getArrayOfItemsForLastNews(12)
,

                    // HTML attribute for every carousel item

                    // settings for js plugin
                    // @see http://kenwheeler.github.io/slick/#settings
                    'clientOptions' => [
                        'autoplay' => false,
                        'dots'     => false,
                        'arrows' => true,
                        'slidesToShow' => 3,
                        'slidesToScroll' => 1,
                        'prevArrow' => '<li class="previous prev-slick-slider"><a href="#">&larr; '. Yii::t('yii', 'Previous') .'</a></li>',
                        'nextArrow' => '<li class="next next-slick-slider"><a href="#">'. Yii::t('yii', 'Next') .' &rarr;</a></li>',
                        'appendArrows' => '.pager-slick-slider',
                        'centerMode' => false,
                        'draggable' => true,
                        'responsive' => [
                            [
                                'breakpoint' => 1200,
                                'settings' => [
                                    'slidesToShow' => 3,
                                    'slidesToScroll' => 1,
                                ],
                            ],
                            [
                                'breakpoint' => 992,
                                'settings' => [
                                    'slidesToShow' => 2,
                                    'slidesToScroll' => 1,
                                ],
                            ],
                            [
                                'breakpoint' => 768,
                                'settings' => [
                                    'slidesToShow' => 2,
                                    'slidesToScroll' => 1,
                                ],
                            ],
                            [
                                'breakpoint' => 590,
                                'settings' => [
                                    'slidesToShow' => 1,
                                    'slidesToScroll' => 1,
                                ],
                            ],
                        ],
                    ],

                ]); ?>

            <ul class="pager pager-slick-slider">
            </ul>

            <div class="go-to-all-news">
                <?= Html::a(Yii::t('yii','Go to all publications of blog...'), '/blog/') ?>
            </div>

        </div>

        <div class="hr-divider"></div>

    <?php if(!empty($bottomPage)): ?>
    <div class="row jumbotron jumbotron-white" >
        <div class="col-lg-12">
            <h1 class="page-header jumbotron-header">
                <?= $bottomPage->title ?>
            </h1>
        </div>
        <div class="col-xs-12">
            <?= $bottomPage->short_description ?>
            <a class="btn btn-default btn-lg button-mg-bottom" href="/<?= $bottomPage->slug ?>"><?= Yii::t('yii', 'Read more'); ?> <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <?php endif; ?>
</div>