<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('yii','information of user'),
    ]) . ' ' . $model->login;
$this->params['breadcrumbs'][] = $model->login;
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="container wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"><?= $this->title ?></h1>
            <div class="breadcrumbs-outer"></div>
        </div>
    </div>
    <div class="row thumbnail page-content contacts-page clear-margin">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?= $this->render('_userForm', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>

