<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = Yii::t('yii', 'Contacts of KSM department');
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(array(
    'description' => $contactMetaDescription
));

$this->registerMetaTag(array(
    'keyword' => $contactMetaTags
));
?>

<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- /Facebook -->

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="container wrapper">

    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"> <?= $this->title ?></h1>
            <div class="breadcrumbs-outer"></div>
        </div>
    </div>
    <div class="row thumbnail page-content contacts-page clear-margin">
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9">
            <h4><?= Yii::t('yii', 'Address') ?>:</h4>
            <p><?= Yii::t('yii', 'Khmelnytsky National University, 1st academic building, room. 1-106, 112 Kamenetska str, Khmelnytsky city, 29016') ?></p>
            <h4><?= Yii::t('yii', 'Post address') ?>:</h4>
            <p><?= Yii::t('yii', 'Khmelnytsky National University Department of Computer Systems and Networks, 11 Institutska str, 29016, Khmelnytsky city, UKRAINE') ?></p>
            <h4><?= Yii::t('yii', 'Contact numbers') ?>:</h4>
            <p>(0382) 72-76-84</p>
            <p>12-95 (<?= Yii::t('yii', 'internal telephone') ?>)</p>
            <h4><?= Yii::t('yii', 'E-mail') ?></h4>
            <p>ksm.khnu@gmail.com</p>
            <p>ksm@khnu.km.ua</p>
            <h4><?= Yii::t('yii', 'Follow us') ?>:</h4>
            <div class="social-links-bar">
                <a class="social-link" href="https://plus.google.com/100534378521830550230/about" target="_blank"><i class="fa fa-google-plus-square"></i></a>
                <a class="social-link" href="https://www.facebook.com/khnuksm" target="_blank"><i class="fa fa-facebook-square"></i></a>
                    <a class="social-link" href="https://twitter.com/ksm_khnu" target="_blank"><i class="fa fa-twitter-square"></i></a>
                    <a class="social-link" href="https://www.youtube.com/channel/UCl4U3G90mB4lqu9_Ao6s2QA" target="_blank"><i class="fa fa-youtube-play"></i></a>
                    <a class="social-link" href="http://vk.com/club95427377" target="_blank"><i class="fa fa-vk"></i></a>
            </div>
            <div class="row cont-us-button">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                    <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#modal-contact-us"><?= Yii::t('yii', 'Send message') ?></button>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="row social-widget">
                <div class="col-xs-12 cos-sm-12 col-md-12 col-md-12">
                    <div class="g-page" data-width="300" data-href="https://plus.google.com/100534378521830550230" data-layout="landscape" data-rel="publisher"></div>
                </div>
            </div>
        </div>

        <div class="row social-widgets">
            <div class="col-xs-12 cos-sm-6 col-md-4 col-md-3">
                <div class="hr-divider"></div>
                <div class="row">
                    <div class="col-xs-12 cos-sm-12 col-md-12 col-md-12">
                        <div class="fb-page" data-href="https://www.facebook.com/khnuksm" data-width="245" data-height="100" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/khnuksm"><a href="https://www.facebook.com/khnuksm">Кафедра комп&#039;ютерних систем та мереж ХНУ</a></blockquote></div></div>
                    </div>
                </div>
                <div class="hr-divider"></div>
                <div class="row">
                    <div class="col-xs-12 cos-sm-12 col-md-12 col-md-12">
                        <div>
                            <a class="twitter-timeline" href="https://twitter.com/ksm_khnu" data-widget-id="612887660814835712"><?= Yii::t('yii', 'Twits from user') ?> @ksm_khnu</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                    </div>
                </div>
                <div class="hr-divider"></div>
                <div class="row">
                    <div class="col-xs-12 cos-sm-12 col-md-12 col-md-12">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode: 0, width: "245", height: "100", color1: 'FFFFFF', color2: '333', color3: 'B6B6B6'}, 95427377);
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <div class="hr-divider"></div>
        <iframe class="col-xs-12 col-sm-12 col-md-12 col-lg-12" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1298.0811652363452!2d26.964526463821795!3d49.40583577847999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47320688778ad6af%3A0x47638e7e4a9a610!2z0JrQsNC8J9GP0L3QtdGG0YzQutCwINCy0YPQuy4sIDExMiwg0KXQvNC10LvRjNC90LjRhtGM0LrQuNC5INCd0LDRhtGW0L7QvdCw0LvRjNC90LjQuSDQo9C90ZbQstC10YDRgdC40YLQtdGCLCDQpdC80LXQu9GM0L3QuNGG0YzQutC40LksINCl0LzQtdC70YzQvdC40YbRjNC60LAg0L7QsdC70LDRgdGC0Yw!5e0!3m2!1suk!2sua!4v1433864284504" width="600" height="450" frameborder="0" style="border:0"></iframe>
    </div>
</div>


<div class="modal fade" id="modal-contact-us" tobindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content thumbnail-feedback">
            <div class="modal-header feedback-form-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><span class="glyphicon glyphicon-send"> </span> <?= Yii::t('yii', 'Feedback form') ?> </h3>
            </div>
            <div class="modal-body modal-feedback">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 has-feedback form-groupp">
                        <p>
                            <?= Yii::t('yii', 'If you have some inquiries or questions, please fill out the following form to contact us. Thank you.') ?>
                        </p>
                        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                        <?= $form->field($model, 'name') ?>
                        <?= $form->field($model, 'email') ?>
                        <?= $form->field($model, 'subject') ?>
                        <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                        ]) ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('yii', 'Submit'), ['class' => 'btn btn-default', 'name' => 'contact-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Google -->
<script src="https://apis.google.com/js/platform.js" async defer>{lang: 'uk'}</script>