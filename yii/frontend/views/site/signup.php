<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = Yii::t('yii', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="container wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"><?= $this->title ?>
                <small></small>
            </h1>
            <div class="breadcrumbs-outer"></div>
        </div>
    </div>
    <div class="row clear-margin">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-md-offset-2 col-lg-offset-2 thumbnail thumbnail-form">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <h3><?= Html::encode($this->title) ?></h3>
                    <div class="hr-divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">

                    <p><?php echo Yii::t('yii', 'Please fill out the following fields to signup:') ?></p>

                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                        <?= $form->field($model, 'login') ?>
                        <?= $form->field($model, 'email') ?>
                        <?= $form->field($model, 'password')->passwordInput() ?>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label class="checkbox-inline">
                                    <input type="checkbox" required="required" value="agree" id="check-agree"> <?= Yii::t('yii', 'I agree with ') ?> <a href="" data-toggle="modal" data-target="#modal-registration-rules" > <?= Yii::t('yii', 'registration rules') ?> правилами реєстрації</a>.
                                    <label class="control-label error-agree hidden"> <?= Yii::t('yii', 'You have to read and confirm the registration rules to complete this step.') ?> </label>
                                </label>

                            </div>
                        </div>
                        <br/><br/>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('yii', 'Signup'), ['class' => 'btn btn-default', 'id' => 'send-registration', 'name' => 'signup-button']) ?>
                            <?= Html::button(Yii::t('yii', 'Clear the form'), ['class' => 'btn btn-default', 'type' => 'reset' ]) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-registration-rules" tobindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content thumbnail-form">
            <div class="modal-header feedback-form-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><span class="glyphicon glyphicon-info-sign"> </span> <?= Yii::t('yii', 'Registration rules') ?></h3>
            </div>
            <div class="modal-body modal-feedback">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 has-feedback form-groupp">
                        <?= $registrationRules; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 ">
                        <button type="button" id="agree-registration" class="btn btn-success btn-md"><?= Yii::t('yii', 'Agree') ?></button>
                        <button type="button" id="disagree-registration" class="btn btn-danger btn-md"><?= Yii::t('yii', 'Disagree') ?></button>
                        <div class="clr"></div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

