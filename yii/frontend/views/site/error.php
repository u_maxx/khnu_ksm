<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>


<div class="container wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"><?= $page->title ?></h1>
        </div>
    </div>
    <div class="row thumbnail page-content contacts-page clear-margin">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thumbnail-form">
            <div class="hr-divider"></div>
            <div class="site-error">

                <h1><?= Html::encode($this->title) ?></h1>

                <div class="alert alert-danger">
                    <?= nl2br(Html::encode($message)) ?>
                </div>

                <p>
                    <?= Yii::t('yii', 'The above error occurred while the Web server was processing your request.') ?>
                </p>
                <p>
                    <?= Yii::t('yii', 'Please contact us if you think this is a server error. Thank you.') ?>
                </p>

            </div>
            <div class="hr-divider"></div>
        </div>
    </div>
</div>


