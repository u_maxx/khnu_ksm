<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = Yii::t('yii', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9 col-sm-6 col-md-4 col-lg-3 col-xs-offset-3 col-sm-offset-6 col-md-offset-8 .col-lg-offset-9 flash-outer"></div>

<div class="container wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?>
            </h1>
            <div class="breadcrumbs-outer"></div>
        </div>
    </div>
    <div class="row clear-margin">
        <div class="col-xs-12 col-sm-10 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-1 col-md-offset-3 col-lg-offset-3 thumbnail thumbnail-form">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <h3><?= Html::encode($this->title) ?></h3>
                    <div class="hr-divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <p><?= Yii::t('yii', 'Please fill out your email. A link to reset password will be sent there.') ?></p>
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                        <?= $form->field($model, 'email') ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('yii', 'Send'), ['class' => 'btn btn-default']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
