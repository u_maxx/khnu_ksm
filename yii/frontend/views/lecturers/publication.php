<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 09.10.15
 * Time: 21:54
 */

$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Lecturers'), 'url' => ['/lecturers/index']];
$this->params['breadcrumbs'][] = Yii::t('yii', 'Publications');
$this->params['breadcrumbs'][] = $publication->title;
?>

<div class="container wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"><?= $publication->title ?> <small><?= $publication->author ?></small></h1>
            <div class="breadcrumbs-outer"></div>
        </div>
    </div>
    <div class="row thumbnail page-content contacts-page clear-margin">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <p class="lead">
                <?= $publication->author .' - '. $publication->title . ' / ' . $publication->publishing_house . ', ' . $publication->year . 'р. – c.' . $publication->cout_page; ?>
            </p>
            <div class="hr-divider"></div>
            <?= $publication->about ?>
            <div class="hr-divider"></div>
        </div>
    </div>
</div>