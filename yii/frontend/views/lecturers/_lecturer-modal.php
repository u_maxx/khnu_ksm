<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.10.15
 * Time: 17:44
 */

use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\models\SubjectsToTeachers;
use common\models\ScActivitiesToTeachers;
use common\models\PublicationsToTeachers;
use yii\helpers\Html;



$scActivityDataProvider = new ActiveDataProvider([
    'query' => ScActivitiesToTeachers::find()->where(['teacher_id' => $model->id])
]);

$subjectsDataProvider = new ActiveDataProvider([
    'query' => SubjectsToTeachers::find()->where(['teacher_id' => $model->id])
]);

$publicationsDataProvider = new ActiveDataProvider([
    'query' => PublicationsToTeachers::find()->where(['teacher_id' => $model->id])
]);

?>

<div class="modal fade lector-model" id="modal-<?= $model->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><span class="glyphicon glyphicon glyphicon-user"> </span> <?= $model->name ?></h3>
                <small><?= $model->degree ?></small>
            </div>
            <div class="modal-body about-lector-content">


                <?php if($model->image): ?>
                    <?= '<div class="image-border">' ?>
                    <?= Html::img('@web/uploads/teachers-photos/'.$model->image, ['alt' => $model->name]) ?>
                    <?= '</div>' ?>
                <?php endif ?>
                <?= $model->about ?>

                <div class="about-lector-list">
                    <h3><?= Yii::t('yii', 'Scientific activities'); ?>:</h3>
                    <ul>
                        <?php
                            echo ListView::widget([
                                'dataProvider' => $scActivityDataProvider,
                                'itemView' => '_sc-activity',
                            ]);
                        ?>
                    </ul>
                </div>

                <div class="about-lector-list">
                    <h3><?= Yii::t('yii', 'Subjects'); ?>:</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="lector-<?= $model->id ?>-subjects" class="panel-group">
                                <?php
                                    echo ListView::widget([
                                        'dataProvider' => $subjectsDataProvider,
                                        'itemView' => '_subject',
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-lector-list">
                    <h3><?= Yii::t('yii', 'Last publications'); ?>:</h3>
                    <ul>
                        <?php
                            echo ListView::widget([
                                'dataProvider' => $publicationsDataProvider,
                                'itemView' => '_publication',
                            ]);
                        ?>
                    </ul>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->