<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.10.15
 * Time: 17:44
 */

use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\models\ScActivitiesToTeachers;
use yii\helpers\Html;

$scActivityDataProvider = new ActiveDataProvider([
    'query' => ScActivitiesToTeachers::find()->where(['teacher_id' => $model->id])
]);

?>


<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 lector-tab">
    <div class="thumbnail col-xs-12 col-sm-12 col-md-12 col-lg-12 lector">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 lector-short-header">
                <h3 class="modal-title"><span class="glyphicon glyphicon glyphicon-user"> </span> <?= $model->name ?></h3>
                <small><?= $model->degree ?></small>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                <?php if($model->image): ?>
                    <?= '<div class="image-border">' ?>
                    <?= Html::img('@web/uploads/teachers-photos/'.$model->image, ['alt' => $model->name]) ?>
                    <?= '</div>' ?>
                <?php endif ?>
                <?= $model->about ?>
                <div class="clr"></div>
                <div class="about-lector-list">
                    <h3><?= Yii::t('yii', 'Scientific activities'); ?>:</h3>
                    <ul>
                        <?php
                            echo ListView::widget([
                                'dataProvider' => $scActivityDataProvider,
                                'itemView' => '_sc-activity',
                            ]);
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 lector-button">
                <button type="button" class="btn btn-default button-fl-right" data-toggle="modal" data-target="#modal-<?= $model->id ?>"><?= Yii::t('yii', 'Read more'); ?>...</button>
                <div class="clr"></div>
            </div>
        </div>
    </div>
</div>
