<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.10.15
 * Time: 17:45
 */

use common\models\Subjects;

$subject = Subjects::findOne($model->subject_id);
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-hand-right"> </span>
            <a href="#collapse-<?= $model->teacher_id ?>-<?= $subject->id ?>" data-parent="#lector-<?= $model->teacher_id ?>-subjects" data-toggle="collapse"><?= $subject->name ?></a>
        </h4>
    </div>
    <div id="collapse-<?= $model->teacher_id ?>-<?= $subject->id ?>" class="panel-collapse collapse ">
        <div class="panel-body">
            <p>
                <?= $subject->about ?>
            </p>
        </div>
    </div>
</div>