<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.10.15
 * Time: 17:46
 */

use common\models\Publications;
use yii\helpers\Html;

$publication = Publications::findOne($model->publication_id);

?>

<?php if($publication ->about): ?>
    <?= Html::tag('li',Html::a($publication->author .' - '. $publication->title . ' / ' . $publication->publishing_house . ', ' . $publication->year . 'р. – c.' . $publication->cout_page, [Yii::$app->urlManager->createUrl(['lecturers/publication', 'id' => $publication ->id])]), ['class' => 'publication-point']); ?>
<?php else : ?>
    <?= Html::tag('li',$publication->author .' - '. $publication->title . ' / ' . $publication->publishing_house . ', ' . $publication->year . 'р. – c.' . $publication->cout_page, ['class' => 'publication-point']); ?>
<?php endif; ?>