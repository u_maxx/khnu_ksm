<?php
/* @var $this yii\web\View */

use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

$lecturerDataProvider = new ActiveDataProvider([
    'query' => $lecturers
]);

$this->registerMetaTag(array(
    'description' => $mainMetaDescription
));

$this->registerMetaTag(array(
    'keyword' => $lecturersMetaTags
));

$this->title = Yii::t('yii', 'Lecturers Department of Computer Systems and Networks');
?>

<div class="container lectors-page">
    <div class="row">
        <div class="col-xs-12">
            <h2><?= Yii::t('yii', 'Lecturers Department of Computer Systems and Networks'); ?></h2>
        </div>


        <?= ListView::widget([
            'dataProvider' => $lecturerDataProvider,
            'itemView' => '_lecturer',
        ]) ?>
    </div>
</div>

<div class="lectors-list">
    <?= ListView::widget([
        'dataProvider' => $lecturerDataProvider,
        'itemView' => '_lecturer-modal',
    ]) ?>
</div>