<?php
/* @var $this yii\web\View */
/* @var $page common\models\Page */

$this->title = $siteFullName;

$this->registerMetaTag(array(
    'description' => $page->meta_description
));

$this->registerMetaTag(array(
    'keyword' => $page->meta_keyword
));

?>

<?php echo $page->description ?>
