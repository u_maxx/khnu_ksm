<?php
/* @var $this yii\web\View */
/* @var $page common\models\Page */

$this->title = $page->title;

$this->params['breadcrumbs'][] = $page->title;

$this->registerMetaTag(array(
    'description' => $page->meta_description
));


$this->registerMetaTag(array(
    'keyword' => $page->meta_keyword
));

?>

<div class="container wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header"><?= $page->title ?></h1>
            <div class="breadcrumbs-outer"></div>
        </div>
    </div>
    <div class="cms-container row thumbnail page-content contacts-page clear-margin">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="hr-divider"></div>
            <?= $page->description ?>
            <div class="clr"></div>
            <div class="hr-divider"></div>
        </div>
    </div>
</div>