<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\CustomAsset;
use frontend\assets\MultilevelMenuAsset;
use common\widgets\Alert;
use common\models\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
CustomAsset::register($this);
MultilevelMenuAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    
    <div class="head">
        <?php
            NavBar::begin([
                'brandLabel' => Html::img('@web/images/logo3.png',['class'=>'logo']),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            $indexItem = ['label' => Yii::t('yii', 'Home'), 'url' => ['/site/index']];
            $menuItems = [
                $indexItem,
                ['label' => Yii::t('app', 'Blog'), 'url' => ['#']],
                ['label' => Yii::t('app', 'Department'), 'url' => ['#'],
                     'items' => [
                        ['label' => Yii::t('yii', 'Subjects'), 'url' => ['/subjects/index']],
                        ['label' => Yii::t('yii', 'Lecturers'), 'url' => ['/lecturers/index']],
                        ['label' => Yii::t('app', 'Disciplines Department'), 'url' => ['#']],
                        ['label' => Yii::t('app', 'Scientific activities'), 'url' => ['#']],
                        ['label' => Yii::t('app', 'Awards'), 'url' => ['#']],
                        '<li role="presentation" class="divider"></li>',
                        ['label' => Yii::t('app', 'Education plans'), 'url' => ['#']],
                     ],
                ],
            ];

        $mainMenuItems = Menu::getItems();

            if (Yii::$app->user->isGuest) {
                $userMenuItems[] =
                    ['encode' => false,'label' => Html::img('@web/images/Icon-user.png',['class'=>'user-avatar-nav']),
                     'items' => [
                        ['label' => Yii::t('yii', 'Login'), 'url' => ['/site/login']],
                        ['label' => Yii::t('yii', 'Signup'), 'url' => ['/site/signup']],
                         
                      ],
                    ];
            } else {
                $userMenuItems[] =
                    ['encode' => false,'label' => Html::img('@web/images/Icon-user.png',['class'=>'user-avatar-nav']),
                     'items' => [
                         ['label' => Yii::t('app', 'Edit details'), 'url' => ['/site/user-update']],
                         '<li role="presentation" class="divider"></li>',
                        [
                            'label' => Yii::t('app', 'Logout ({username})', [
                                'username'  =>  Yii::$app->user->identity->login,
                            ]),
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']
                        ],
                      ],
                    ];
            }

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $userMenuItems,
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $mainMenuItems,
            ]);            
            NavBar::end();
        ?>
    </div>

    <div class="container-fluid wrapper-contant">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

    <footer id="footer" class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-sm-5 col-md-3 col-lg-4 visible-sm visible-lg visible-md">
            <div class="icon-bar-footer">
                <a class="social-link-footer" href="https://plus.google.com/100534378521830550230/about" target="_blank"><i class="fa fa-google-plus-square"></i></a>
                <a class="social-link-footer" href="https://www.facebook.com/khnuksm" target="_blank"><i class="fa fa-facebook-square"></i></a>
                <a class="social-link-footer" href="https://twitter.com/ksm_khnu" target="_blank"><i class="fa fa-twitter-square"></i></a>             
                <a class="social-link-footer" href="https://www.youtube.com/channel/UCl4U3G90mB4lqu9_Ao6s2QA" target="_blank"><i class="fa fa-youtube-play"></i></a>
                <a class="social-link-footer" href="http://vk.com/club95427377" target="_blank"><i class="fa fa-vk"></i></a>    
            </div>
            </div>
            <div class="col-xs- col-sm-7 col-md-9 col-lg-4">
            <p class="visible-sm visible-lg visible-md"><a href="/"><?php echo Yii::t('yii', 'Home'); ?></a> | <a href="/istoria-kafedri-ksm"><?php echo Yii::t('app', 'About department'); ?></a> | <a href="/blog/index"><?php echo Yii::t('app', 'News'); ?></a> | <a href="/site/contact"><?php echo Yii::t('yii', 'Contact'); ?></a></p>
            <p>© <?= Yii::t('yii', 'Department of Computer Systems and Networks'); ?> <?= date('Y') ?>.</p>
          </div>
        </div>
    </footer>

    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5579e864056219a6" async="async"></script>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
